/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __IPTC_MATCHES_H__
#define __IPTC_MATCHES_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "iptc_rules.h"

struct xt_entry_match* iptc_rule_add_udp_match(struct iptc_rule* r,
                                               int dport, int dport_max, int dflags,
                                               int sport, int sport_max, int sflags);
struct xt_entry_match* iptc_rule_add_tcp_match(struct iptc_rule* r,
                                               int dport, int dport_max, int dflags,
                                               int sport, int sport_max, int sflags);
struct xt_entry_match* iptc_rule_add_length_match(struct iptc_rule* r, unsigned short min,
                                                  unsigned short max, unsigned char exclude);
struct xt_entry_match* iptc_rule_add_icmp_match(struct iptc_rule* r, unsigned char type,
                                                unsigned char code, unsigned char flags);
struct xt_entry_match* iptc_rule_add_icmp6_match(struct iptc_rule* r, unsigned char type,
                                                 unsigned char code, unsigned char flags);
struct xt_entry_match* iptc_rule_add_dscp_match(struct iptc_rule* r, unsigned char dscp,
                                                unsigned char flags);
struct xt_entry_match* iptc_rule_add_mac_match(struct iptc_rule* r, uint8_t* smac, int sinvert);

struct xt_entry_match* iptc_rule_add_state_match(struct iptc_rule* r, const char* state);

struct xt_entry_match* iptc_rule_add_connbytes_match(struct iptc_rule* r, unsigned long from, unsigned long to, const char* what, const char* direction);

struct xt_entry_match* iptc_rule_add_skbmark_match(struct iptc_rule* r, unsigned int mark, unsigned int mask, unsigned char exclude);

struct xt_entry_match* iptc_rule_add_connmark_match(struct iptc_rule* r, unsigned int mark, unsigned int mask, unsigned char exclude);

struct xt_entry_match* iptc_rule_add_string_match(struct iptc_rule* r, const char* string);

struct xt_entry_match* iptc_rule_add_time_match(struct iptc_rule* r, uint32_t timeStart, uint32_t timeStop, uint8_t weekdays);

struct xt_entry_match* iptc_rule_add_set_match(struct iptc_rule* r, const char* setname, bool source, bool exclude);

#ifdef __cplusplus
}
#endif

#endif   // __IPTC_MATCHES_H__
