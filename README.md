# libfwinterface

[TOC]

# Introduction

Libfwinterface stands for "lib firewall interface" and is developed as an
abstraction layer between `libfwrules` and the actual firewall on the system.

If you don't know what `libfwrules` is, it is recommended to consult that
documentation first, before proceeding.

`Libfwrules` is created as an intermediate format between different data model
layers. The library is used for storing firewall rules in a common format,
suitable for processing later. The firewall rules are not yet applied to the
system.

The Linux kernel can be built with several firewall subsystems, like
[netfilter](https://www.netfilter.org), its successor
[nftables](https://netfilter.org/projects/nftables/) or any other firewall
implementation.

Each implemenation has its own API to create, modify or delete firewall rules.
For `netfilter`, library `libiptc` can be used. `Nftables`, on the other hand,
uses `netlink`. Other implementions can have a different API as well.

When you want to process firewall rules, stored in `libfwrules`, some problems can arise:
- Your application could be ported to another system, running a different
  firewall, meaning you have to switch libraries.
- Your application should have code to support different libraries to interact
  with the firewall. This can polute your code base.
- It is harder to share the code between different applications.

This is where `libfwinterface` comes into place. The library provides an API to
create, modify or delete firewall rules in the system. The API can interface
with different firewall implementations, selected at compile time. Your
application code remains cleaner, since the firewall abstraction layer is moved
to `libfwinterface`.

The next image shows where this library resides in the application.

![libfwinterface](doc/images/libfwinterface.svg)

# Overview

This section provides an overview how `libfwrules` and `libfwinterface`
interact. On the left side, the internal components of `libfwrules` are shown.
If you have read libfwrules' documentation, you recognize the rules, folders,
the linked list g_rules, the fw_commit and callback functions.

![Overview](doc/images/overview.svg)

The callback function is used in combination with libfwrules' Folder API. It
resides in the application, as shown in the image, and is invoked when a
firewall rule is added, modified or deleted. The callback's prototype is:

    typedef bool (* fw_callback_t)(const fw_rule_t* const rule,
                                   const fw_rule_flag_t flag,
                                   const char* chain,
                                   const char* table,
                                   int index);

The arguments to the callback function are:
- rule: the firewall rule that must be processed.
- flag: the state of a rule. This can be new, modified or deleted.
- chain: the rule's chain.
- table: the rule's table.
- index: the index for this rule's table-chain combination.

The application is reponsible for checking the `flag` argument to decide which
`libfwinterface` API function to call. The following functions can be used:

- fw_add_rule(), fw_replace_rule(): adds or replaces a rule to the firewall
  chain.  Used with flag `FW_RULE_FLAG_NEW` or `FW_RULE_FLAG_MODIFIED`.
- fw_append_rule(): appends a rule to the end of a firewall chain. Used with
  `FW_RULE_FLAG_NEW` or `FW_RULE_FLAG_MODIFIED`.
- fw_delete_rule(): deletes a rule from the firewall chain. Used with flag
  `FW_RULE_FLAG_DELETE`.

It is **important** to note that the API functions cache the new configuration. To write this configuration to the firewall, invoke fw_apply().

On the right side of the image, `libfwinterface`, other `libraries` and the
Linux kernel are shown. The orange block in `libfwinterface` represents the API
functions. The yellow blocks are the different interfaces the library supports.
Each implementation must implement the common API functions. The interface is
chosen at compile time, using config option
`CONFIG_SAH_LIB_FWINTERFACE_INTERFACE_<name>`.

Each interface implementation has a dependency to a specific (open source)
library, e.g.:
- The IPTC interface depends on libiptc.
- The Netlink interface depends on libnetlink.
- ...

The libraries communicate with the firewall subsystem, which resides in the
Linux kernel.

@note
A dummy interface is added to give a head start when writing a new implemenation.


# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/libraries/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --cap-add=CAP_NET_RAW \
            --cap-add=CAP_NET_ADMIN \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>Libfwinterface</code> depends on the following libraries:

- libamxc
- libsahtrace
- libfwrules
- libiptc-dev
- iptables

These libraries can be installed in the container:

    sudo apt-get install libamxc mod-sahtrace sah-lib-sahtrace-dev libfwrules libiptc-dev iptables iproute2

The `iproute2` is installed for later purposes and provides the `ip` command.

### Build libfwinterface

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/libraries/
        git clone git@gitlab.com:prpl-foundation/components/core/libraries/libfwinterface.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/libraries/libfwinterface
        make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/libraries/libfwinterface
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/libraries/libfwinterface
    make package

The packages generated are:

    ~/amx/libraries/libfwinterface/libfwinterface-<VERSION>.tar.gz
    ~/amx/libraries/libfwinterface/libfwinterface-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/libraries/libfwinterface/libfwinterface-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `libfwinterface`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/libraries/libfwinterface/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/libraries/libfwinterface/tests
    make run coverage

You can combine both commands:

    cd ~/amx/libraries/libfwinterface
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/libraries/libfwinterface/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/libraries/libfwinterface/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/libraries/libfwinterface$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/libraries/libfwinterface/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/libraries/libfwinterface

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.
- docs directory: contains the Doxygen settings, code examples and additional pages used in Doxygen.

The code itself is documented in the approriate header and source files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/libraries/libfwinterface
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/libraries/libfwinterface/output/html/index.html

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/libraries/libfwinterface$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/libraries/libfwinterface/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the API, code examples, ...

