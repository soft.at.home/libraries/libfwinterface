/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <arpa/inet.h>

#include "test_interface_iptc.h"

#include <fwrules/fw_rule.h>
#include <fwinterface/interface.h>

#include "interface_iptc/iptc.h"
#include "interface_iptc/iptc_rules.h"
#include "interface_iptc/iptc_includes.h"

#include <linux/netfilter/xt_string.h>
#include <linux/netfilter/xt_time.h>
#include <linux/netfilter/xt_set.h>
#include <linux/netfilter/xt_physdev.h>

typedef enum _rule_add_type {
    RULE_INSERT,
    RULE_REPLACE,
    RULE_APPEND
} rule_add_type_t;

struct table_chain {
    char* table;
    char* chains[4];
};

static const struct table_chain table_chains[] = {
    { .table = "filter", .chains = { "INPUT", "FORWARD", "OUTPUT", NULL} },
    { .table = "raw", .chains = { "PREROUTING", "POSTROUTING", NULL, NULL }},
    { .table = "mangle", .chains = { "PREROUTING", "INPUT", "FORWARD", "POSTROUTING" }},
    { .table = "nat", .chains = { "PREROUTING", "OUTPUT", "POSTROUTING", NULL}}
};

static const int n_tc = sizeof(table_chains) / sizeof(table_chains[0]);

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

void test_interface_iptc_add_chain(UNUSED void** state) {
    int retval = -1;
    const struct table_chain* tc = NULL;
    int i = 0;
    int j = 0;
    int n_c = 0;

    will_return_always(__wrap_file_flock, 0);

    retval = fw_add_chain(NULL, NULL, false);
    assert_int_equal(retval, -1);

    retval = fw_add_chain("PREROUTING", NULL, false);
    assert_int_equal(retval, -1);

    retval = fw_add_chain(NULL, "nat", false);
    assert_int_equal(retval, -1);

    retval = fw_add_chain("INPUT", NULL, true);
    assert_int_equal(retval, -1);

    retval = fw_add_chain(NULL, "mangle", true);
    assert_int_equal(retval, -1);

    will_return_always(__wrap_iptc_commit, 1);
    will_return_always(__wrap_ip6tc_commit, 1);

    for(i = 0; i < n_tc; i++) {
        char* t = NULL;

        tc = &table_chains[i];

        t = tc->table;
        n_c = sizeof(tc->chains) / sizeof(tc->chains[0]);

        expect_string(__wrap_iptc_init, table, t);
        expect_string(__wrap_ip6tc_init, table, t);

        for(j = 0; j < n_c; j++) {
            char* c = tc->chains[j];

            if(!c) {
                break;
            }

            //Good, IPv4
            expect_string(__wrap_iptc_create_chain, chain, c);
            will_return(__wrap_iptc_create_chain, 1);
            retval = fw_add_chain(c, t, false);
            assert_int_equal(retval, 0);

            //Bad, IPv4
            expect_string(__wrap_iptc_create_chain, chain, c);
            will_return(__wrap_iptc_create_chain, 0);
            retval = fw_add_chain(c, t, false);
            assert_int_equal(retval, -1);

            //Bad, IPv4, errno
            expect_string(__wrap_iptc_create_chain, chain, c);
            will_return(__wrap_iptc_create_chain, 0);
            expect_string(__wrap_iptc_flush_entries, chain, c);
            will_return(__wrap_iptc_flush_entries, 1);
            errno = EEXIST;
            retval = fw_add_chain(c, t, false);
            assert_int_equal(retval, 0);
            errno = 0;

            //Good, IPv6
            expect_string(__wrap_ip6tc_create_chain, chain, c);
            will_return(__wrap_ip6tc_create_chain, 1);
            retval = fw_add_chain(c, t, true);
            assert_int_equal(retval, 0);

            //Bad, IPv6
            expect_string(__wrap_ip6tc_create_chain, chain, c);
            will_return(__wrap_ip6tc_create_chain, 0);
            retval = fw_add_chain(c, t, true);
            assert_int_equal(retval, -1);

            //Bad, IPv6, errno
            expect_string(__wrap_ip6tc_create_chain, chain, c);
            will_return(__wrap_ip6tc_create_chain, 0);
            expect_string(__wrap_ip6tc_flush_entries, chain, c);
            will_return(__wrap_ip6tc_flush_entries, 1);
            errno = EEXIST;
            retval = fw_add_chain(c, t, true);
            assert_int_equal(retval, 0);
            errno = 0;

            c++;
        }
    }

    fw_apply();
}

static void test_interface_iptc_add_replace_append_rule(rule_add_type_t type) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    int index = 1;
    int is_ipv6 = 0;
    int good = 0;
    const struct table_chain* tc = NULL;
    int i = 0;
    int j = 0;
    int n_c = 0;

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    switch(type) {
    case RULE_INSERT:
        retval = fw_add_rule(NULL, 1);
        assert_int_equal(retval, -1);

        retval = fw_add_rule(rule, 0);
        assert_int_equal(retval, -1);

        break;

    case RULE_REPLACE:
        retval = fw_replace_rule(NULL, 1);
        assert_int_equal(retval, -1);

        retval = fw_replace_rule(rule, 0);
        assert_int_equal(retval, -1);

        break;

    case RULE_APPEND:
        retval = fw_append_rule(NULL);
        assert_int_equal(retval, -1);

        break;
    }

    will_return_always(__wrap_iptc_commit, 1);
    will_return_always(__wrap_ip6tc_commit, 1);

    for(i = 0; i < n_tc; i++) {
        char* t = NULL;

        tc = &table_chains[i];
        t = tc->table;
        n_c = sizeof(tc->chains) / sizeof(tc->chains[0]);

        for(is_ipv6 = 0; is_ipv6 <= 1; is_ipv6++) {
            const bool ipv6 = (bool) (is_ipv6 == 1);

            if(ipv6) {
                expect_string(__wrap_ip6tc_init, table, t);
            } else {
                expect_string(__wrap_iptc_init, table, t);
            }

            for(j = 0; j < n_c; j++) {
                char* c = tc->chains[j];

                if(!c) {
                    break;
                }
                index = 1;

                retval = fw_rule_set_table(rule, t);
                assert_int_equal(retval, 0);

                retval = fw_rule_set_chain(rule, c);
                assert_int_equal(retval, 0);

                retval = fw_rule_set_ipv6(rule, ipv6);
                assert_int_equal(retval, 0);

                retval = fw_rule_set_target_return(rule);
                assert_int_equal(retval, 0);

                for(good = 0; good <= 1; good++) {

                    switch(type) {
                    case RULE_INSERT:
                        if(ipv6) {
                            expect_string(__wrap_ip6tc_insert_entry, chain, c);
                            will_return(__wrap_ip6tc_insert_entry, good);
                        } else {
                            expect_string(__wrap_iptc_insert_entry, chain, c);
                            will_return(__wrap_iptc_insert_entry, good);
                        }
                        retval = fw_add_rule(rule, index);
                        break;

                    case RULE_REPLACE:     // Makes use of ip(6)tc_insert_entry.
                        if(ipv6) {
                            expect_string(__wrap_ip6tc_insert_entry, chain, c);
                            will_return(__wrap_ip6tc_insert_entry, good);
                        } else {
                            expect_string(__wrap_iptc_insert_entry, chain, c);
                            will_return(__wrap_iptc_insert_entry, good);
                        }
                        retval = fw_replace_rule(rule, index);
                        break;

                    case RULE_APPEND:
                        if(ipv6) {
                            expect_string(__wrap_ip6tc_append_entry, chain, c);
                            will_return(__wrap_ip6tc_append_entry, good);
                        } else {
                            expect_string(__wrap_iptc_append_entry, chain, c);
                            will_return(__wrap_iptc_append_entry, good);
                        }
                        retval = fw_append_rule(rule);
                        break;
                    }
                    assert_int_equal(retval, (good ? 0 : -1));
                    index++;
                }
            }
        }
    }

    fw_apply();

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

void test_interface_iptc_add_rule(UNUSED void** state) {
    will_return_always(__wrap_file_flock, 0);
    test_interface_iptc_add_replace_append_rule(RULE_INSERT);
}

void test_interface_iptc_replace_rule(UNUSED void** state) {
    will_return_always(__wrap_file_flock, 0);
    test_interface_iptc_add_replace_append_rule(RULE_REPLACE);
}

void test_interface_iptc_append_rule(UNUSED void** state) {
    will_return_always(__wrap_file_flock, 0);
    test_interface_iptc_add_replace_append_rule(RULE_APPEND);
}

void test_interface_iptc_delete_rule(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    int index = 1;
    int is_ipv6 = 0;
    int good = 0;

    will_return_always(__wrap_iptc_commit, 1);
    will_return_always(__wrap_ip6tc_commit, 1);
    will_return_always(__wrap_file_flock, 0);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);

    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);

    retval = fw_delete_rule(NULL, 1);
    assert_int_equal(retval, -1);

    retval = fw_delete_rule(rule, 0);
    assert_int_equal(retval, -1);

    retval = fw_delete_rule(rule, 0);
    assert_int_equal(retval, -1);

    expect_string(__wrap_iptc_init, table, fw_rule_get_table(rule));
    expect_string(__wrap_ip6tc_init, table, fw_rule_get_table(rule));

    for(is_ipv6 = 0; is_ipv6 <= 1; is_ipv6++) {
        fw_rule_set_ipv6(rule, (bool) is_ipv6);

        for(good = 0; good <= 1; good++) {

            if(is_ipv6) {
                expect_string(__wrap_ip6tc_delete_num_entry, chain, fw_rule_get_chain(rule));
                will_return(__wrap_ip6tc_delete_num_entry, good);
            } else {
                expect_string(__wrap_iptc_delete_num_entry, chain, fw_rule_get_chain(rule));
                will_return(__wrap_iptc_delete_num_entry, good);
            }

            retval = fw_delete_rule(rule, index);
            assert_int_equal(retval, (good ? 0 : -1));
        }
    }

    fw_apply();

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);

}

void test_interface_iptc_apply(UNUSED void** state) {
    fw_apply();
}

static void test_interface_iptc_create_ipv4_rule1(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    struct xt_standard_target* target = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct xt_tcp* tcp_info = NULL;
    struct element* element = NULL;
    struct xt_entry_match* match_entry = NULL;


    // Drop TCP traffic on interface eth1, coming from address 25.84.57.69.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_in_interface(rule, "eth1");
    retval |= fw_rule_set_source(rule, "25.84.57.69");
    retval |= fw_rule_set_protocol(rule, 6); //TCP
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry->ip.iniface, fw_rule_get_in_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_memory_equal(r->e.entry->ip.iniface_mask, intf_mask,
                        strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_VIA_IN, 0);
    assert_int_equal(r->e.entry->ip.src.s_addr, 0x45395419);
    assert_int_equal(r->e.entry->ip.smsk.s_addr, 0xffffffff);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_SRCIP, 0);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    match_entry = (struct xt_entry_match*) element->elem;
    assert_non_null(match_entry);
    tcp_info = (struct xt_tcp*) match_entry->data;
    assert_non_null(tcp_info);
    assert_int_equal((int) tcp_info->spts[0], 0);
    assert_int_equal((int) tcp_info->spts[1], 0xffff);
    assert_int_equal((int) tcp_info->dpts[0], 0);
    assert_int_equal((int) tcp_info->dpts[1], 0xffff);
    assert_int_equal(tcp_info->invflags, 0);

    target = (struct xt_standard_target*) r->t.target;
    assert_non_null(target);
    assert_int_equal(target->verdict, -NF_DROP - 1);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //From source port 32500 to destination port 22.
    retval |= fw_rule_set_source_port(rule, 32500);
    retval |= fw_rule_set_destination_port(rule, 22);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    match_entry = (struct xt_entry_match*) element->elem;
    assert_non_null(match_entry);
    tcp_info = (struct xt_tcp*) match_entry->data;
    assert_non_null(tcp_info);
    assert_int_equal((int) tcp_info->spts[0], 32500);
    assert_int_equal((int) tcp_info->spts[1], 32500);
    assert_int_equal(tcp_info->invflags, 0);
    assert_int_equal((int) tcp_info->dpts[0], 22);
    assert_int_equal((int) tcp_info->dpts[1], 22);
    assert_int_equal(tcp_info->invflags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //From source port range 32500-33000 to destination port range 22-522.
    retval |= fw_rule_set_source_port(rule, 32500);
    retval |= fw_rule_set_source_port_range_max(rule, 33000);
    retval |= fw_rule_set_destination_port(rule, 22);
    retval |= fw_rule_set_destination_port_range_max(rule, 522);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    match_entry = (struct xt_entry_match*) element->elem;
    assert_non_null(match_entry);
    tcp_info = (struct xt_tcp*) match_entry->data;
    assert_non_null(tcp_info);
    assert_int_equal((int) tcp_info->spts[0], 32500);
    assert_int_equal((int) tcp_info->spts[1], 33000);
    assert_int_equal(tcp_info->invflags, 0);
    assert_int_equal((int) tcp_info->dpts[0], 22);
    assert_int_equal((int) tcp_info->dpts[1], 522);
    assert_int_equal(tcp_info->invflags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule2(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    struct xt_standard_target* target = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_udp* udp_info = NULL;
    struct xt_entry_match* entry_match = NULL;

    //Accept forwarded UDP traffic on all interfaces, except eth2, excluding network 192.168.80.0/24.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_in_interface(rule, "eth2");
    retval |= fw_rule_set_in_interface_excluded(rule, true);
    retval |= fw_rule_set_source(rule, "192.168.80.0");
    retval |= fw_rule_set_source_mask(rule, "255.255.255.0");
    retval |= fw_rule_set_source_excluded(rule, true);
    retval |= fw_rule_set_protocol(rule, 17); //UDP
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry->ip.iniface, fw_rule_get_in_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_memory_equal(r->e.entry->ip.iniface_mask, intf_mask,
                        strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_VIA_IN, IPT_INV_VIA_IN);
    assert_int_equal(r->e.entry->ip.src.s_addr, 0x50a8c0);
    assert_int_equal(r->e.entry->ip.smsk.s_addr, 0x00ffffff);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_SRCIP, IPT_INV_SRCIP);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    udp_info = (struct xt_udp*) entry_match->data;
    assert_non_null(udp_info);
    assert_int_equal(udp_info->spts[0], 0);
    assert_int_equal(udp_info->spts[1], 0xffff);
    assert_int_equal(udp_info->dpts[0], 0);
    assert_int_equal(udp_info->dpts[1], 0xffff);
    assert_int_equal(udp_info->invflags, 0);
    assert_string_equal(r->t.target->u.user.name, "ACCEPT");

    target = (struct xt_standard_target*) r->t.target;
    assert_non_null(target);
    assert_int_equal(target->verdict, -NF_ACCEPT - 1);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //From source port 25000 to destination port 8080.
    retval |= fw_rule_set_source_port(rule, 25000);
    retval |= fw_rule_set_destination_port(rule, 8080);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    udp_info = (struct xt_udp*) entry_match->data;
    assert_non_null(udp_info);
    assert_int_equal(udp_info->spts[0], 25000);
    assert_int_equal(udp_info->spts[1], 25000);
    assert_int_equal(udp_info->dpts[0], 8080);
    assert_int_equal(udp_info->dpts[1], 8080);
    assert_int_equal(udp_info->invflags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //From source port range 25000-26000 to destination port range 8080-9080.
    retval |= fw_rule_set_source_port(rule, 25000);
    retval |= fw_rule_set_source_port_range_max(rule, 26000);
    retval |= fw_rule_set_destination_port(rule, 8080);
    retval |= fw_rule_set_destination_port_range_max(rule, 9080);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    udp_info = (struct xt_udp*) entry_match->data;
    assert_non_null(udp_info);
    assert_int_equal(udp_info->spts[0], 25000);
    assert_int_equal(udp_info->spts[1], 26000);
    assert_int_equal(udp_info->dpts[0], 8080);
    assert_int_equal(udp_info->dpts[1], 9080);
    assert_int_equal(udp_info->invflags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule3(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    struct xt_standard_target* target = NULL;

    //Drop all outgoing traffic to 172.217.17.110 on interface wwan0.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "OUTPUT");
    retval |= fw_rule_set_destination(rule, "172.217.17.110");
    retval |= fw_rule_set_out_interface(rule, "wwan0");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry->ip.outiface, fw_rule_get_out_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_out_interface(rule)) + 1);
    assert_memory_equal(r->e.entry->ip.outiface_mask, intf_mask,
                        strlen(fw_rule_get_out_interface(rule)) + 1);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_VIA_IN, 0);
    assert_int_equal(r->e.entry->ip.dst.s_addr, 0x6e11d9ac);
    assert_int_equal(r->e.entry->ip.dmsk.s_addr, 0xffffffff);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_DSTIP, 0);
    assert_string_equal(r->t.target->u.user.name, "DROP");

    target = (struct xt_standard_target*) r->t.target;
    assert_non_null(target);
    assert_int_equal(target->verdict, -NF_DROP - 1);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule4(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct ipt_reject_info* reject = NULL;

    //Reject all forwarded traffic not coming from eth3
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_in_interface(rule, "eth3");
    retval |= fw_rule_set_in_interface_excluded(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry->ip.iniface, fw_rule_get_in_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_memory_equal(r->e.entry->ip.iniface_mask, intf_mask,
                        strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_VIA_IN, IPT_INV_VIA_IN);
    assert_int_equal(r->e.entry->ip.src.s_addr, 0);
    assert_int_equal(r->e.entry->ip.smsk.s_addr, 0);
    assert_int_equal(r->e.entry->ip.dst.s_addr, 0);
    assert_int_equal(r->e.entry->ip.dmsk.s_addr, 0);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_SRCIP, 0);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_DSTIP, 0);
    assert_string_equal(r->t.target->u.user.name, "REJECT");

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    reject = (struct ipt_reject_info*) element->elem;
    assert_non_null(reject);
    assert_int_equal(reject->with, IPT_ICMP_PORT_UNREACHABLE); //Default UDP

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Switch to TCP.
    retval |= fw_rule_set_protocol(rule, 6);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    reject = (struct ipt_reject_info*) element->elem;
    assert_non_null(reject);
    assert_int_equal(reject->with, IPT_TCP_RESET);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule5(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Exclude destination, exclude out interface, exclude TCP
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "OUTPUT");
    retval |= fw_rule_set_destination(rule, "192.168.84.10");
    retval |= fw_rule_set_destination_excluded(rule, true);
    retval |= fw_rule_set_out_interface(rule, "eth0");
    retval |= fw_rule_set_out_interface_excluded(rule, true);
    retval |= fw_rule_set_protocol(rule, 6);
    retval |= fw_rule_set_protocol_excluded(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->t.target->u.user.name, "ACCEPT");
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_DSTIP, IPT_INV_DSTIP);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_VIA_OUT, IPT_INV_VIA_OUT);
    assert_int_equal(r->e.entry->ip.invflags & IPT_INV_PROTO, IPT_INV_PROTO);
    assert_int_equal(r->e.entry->ip.proto, fw_rule_get_protocol(rule));
    assert_int_equal(r->e.entry->ip.flags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule6(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Source and destination masks.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_source(rule, "10.40.0.0/16");
    retval |= fw_rule_set_destination(rule, "192.168.30.0/24");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry->ip.src.s_addr, 0x0000280a);
    assert_int_equal(r->e.entry->ip.smsk.s_addr, 0x0000ffff);
    assert_int_equal(r->e.entry->ip.dst.s_addr, 0x001ea8c0);
    assert_int_equal(r->e.entry->ip.dmsk.s_addr, 0x00ffffff);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Source and destination masks, but masks are 0.
    retval |= fw_rule_set_source(rule, "10.40.0.0/0");
    retval |= fw_rule_set_destination(rule, "192.168.30.0/0");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry->ip.src.s_addr, 0);
    assert_int_equal(r->e.entry->ip.smsk.s_addr, 0);
    assert_int_equal(r->e.entry->ip.dst.s_addr, 0);
    assert_int_equal(r->e.entry->ip.dmsk.s_addr, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule7(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Reject all traffic on eth1, but forget to set a target policy.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_in_interface(rule, "eth1");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule8(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct xt_classify_target_info* pbit_info = NULL;

    //Set pbit to 6.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "POSTROUTING");
    retval |= fw_rule_set_target_pbit(rule, 6);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "CLASSIFY");
    pbit_info = (struct xt_classify_target_info*) element->elem;
    assert_non_null(pbit_info);
    assert_int_equal(pbit_info->priority, fw_rule_get_target_pbit_option(rule));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule9(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Jump to chain PREROUTING_class for all tcp traffic.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_protocol(rule, 6);
    retval |= fw_rule_set_target_chain(rule, "PREROUTING_class");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_non_null(r->t.target);
    assert_string_equal(r->t.target->u.user.name, fw_rule_get_target_chain_option(rule));
    assert_int_equal(r->target_list_size, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule10(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Redirect TCP traffic to destination port 32500 to port 80.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_protocol(rule, 6);
    //retval |= fw_rule_set_target_redirect(rule, "PREROUTING_class");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //TODO: expand test when redirect is added in lib_fwrules.

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule11(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct nf_nat_range2* mr = NULL;
    struct in_addr addr = {0};

    //DNAT: change destination address from 192.168.30.10 to 192.168.50.10
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "nat");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_destination(rule, "192.168.30.10");
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_aton("192.168.50.10", &addr), 1);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DNAT");
    assert_int_equal(r->t.target->u.user.revision, 2);
    mr = (struct nf_nat_range2*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->flags & 0x1, 1);
    assert_int_equal(mr->min_addr.ip, addr.s_addr);
    assert_int_equal(mr->max_addr.ip, addr.s_addr);
    assert_int_equal(mr->flags & IP_NAT_RANGE_PROTO_SPECIFIED, 0);
    assert_int_equal(mr->min_proto.tcp.port, 0);
    assert_int_equal(mr->max_proto.tcp.port, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //DNAT cidr prefix length 32
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10/32", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DNAT");
    assert_int_equal(r->t.target->u.user.revision, 2);
    mr = (struct nf_nat_range2*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->flags & 0x1, 1);
    assert_int_equal(mr->min_addr.ip, addr.s_addr);
    assert_int_equal(mr->max_addr.ip, addr.s_addr);
    assert_int_equal(mr->flags & IP_NAT_RANGE_PROTO_SPECIFIED, 0);
    assert_int_equal(mr->min_proto.tcp.port, 0);
    assert_int_equal(mr->max_proto.tcp.port, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Wrong DNAT IP format.
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10.19", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //Wrong DNAT IP format (only prefixlen 32 allowed for ipv4).
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10/24", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //DNAT ports 80 to 1200
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10", 80, 1200);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DNAT");
    assert_int_equal(r->t.target->u.user.revision, 2);
    mr = (struct nf_nat_range2*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->flags & 0x1, 1);
    assert_int_equal(mr->min_addr.ip, addr.s_addr);
    assert_int_equal(mr->max_addr.ip, addr.s_addr);
    assert_int_equal(mr->flags & IP_NAT_RANGE_PROTO_SPECIFIED, IP_NAT_RANGE_PROTO_SPECIFIED);
    assert_int_equal(mr->min_proto.tcp.port, htons(80));
    assert_int_equal(mr->max_proto.tcp.port, htons(1200));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //DNAT ports 80 to 70: max port will equal min port.
    retval |= fw_rule_set_target_dnat(rule, "192.168.50.10", 80, 70);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DNAT");
    assert_int_equal(r->t.target->u.user.revision, 2);
    mr = (struct nf_nat_range2*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->flags & 0x1, 1);
    assert_int_equal(mr->min_addr.ip, addr.s_addr);
    assert_int_equal(mr->max_addr.ip, addr.s_addr);
    assert_int_equal(mr->flags & IP_NAT_RANGE_PROTO_SPECIFIED, IP_NAT_RANGE_PROTO_SPECIFIED);
    assert_int_equal(mr->min_proto.tcp.port, htons(80));
    assert_int_equal(mr->max_proto.tcp.port, htons(80));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule12(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    MULTI_RANGE* mr = NULL;
    struct in_addr addr = {0};

    //SNAT: change destination address from 192.168.30.10 to 192.168.50.10
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "nat");
    retval |= fw_rule_set_chain(rule, "POSTROUTING");
    retval |= fw_rule_set_destination(rule, "192.168.30.10");
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_aton("192.168.50.10", &addr), 1);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "SNAT");
    mr = (MULTI_RANGE*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->rangesize, 1);
    assert_int_equal(mr->range[0].flags & 0x1, 1);
    assert_int_equal(mr->range[0].min_ip, addr.s_addr);
    assert_int_equal(mr->range[0].max_ip, addr.s_addr);
    assert_int_equal(mr->range[0].flags & IP_NAT_RANGE_PROTO_SPECIFIED, 0);
    assert_int_equal(mr->range[0].min.tcp.port, 0);
    assert_int_equal(mr->range[0].max.tcp.port, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    // SNAT IP prefix length 32
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10/32", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "SNAT");
    mr = (MULTI_RANGE*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->rangesize, 1);
    assert_int_equal(mr->range[0].flags & 0x1, 1);
    assert_int_equal(mr->range[0].min_ip, addr.s_addr);
    assert_int_equal(mr->range[0].max_ip, addr.s_addr);
    assert_int_equal(mr->range[0].flags & IP_NAT_RANGE_PROTO_SPECIFIED, 0);
    assert_int_equal(ntohs(mr->range[0].min.tcp.port), 0);
    assert_int_equal(ntohs(mr->range[0].max.tcp.port), 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Wrong SNAT IP format.
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10.19", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //Wrong SNAT IP prefix length.
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10/24", 0, 0);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //SNAT ports 80 to 1200
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10", 80, 1200);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "SNAT");
    mr = (MULTI_RANGE*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->rangesize, 1);
    assert_int_equal(mr->range[0].flags & 0x1, 1);
    assert_int_equal(mr->range[0].min_ip, addr.s_addr);
    assert_int_equal(mr->range[0].max_ip, addr.s_addr);
    assert_int_equal(mr->range[0].flags & IP_NAT_RANGE_PROTO_SPECIFIED, IP_NAT_RANGE_PROTO_SPECIFIED);
    assert_int_equal(ntohs(mr->range[0].min.tcp.port), 80);
    assert_int_equal(ntohs(mr->range[0].max.tcp.port), 1200);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //SNAT ports 80 to 70: max port will equal min port.
    retval |= fw_rule_set_target_snat(rule, "192.168.50.10", 80, 70);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "SNAT");
    mr = (MULTI_RANGE*) element->elem;
    assert_non_null(mr);
    assert_int_equal(mr->rangesize, 1);
    assert_int_equal(mr->range[0].flags & 0x1, 1);
    assert_int_equal(mr->range[0].min_ip, addr.s_addr);
    assert_int_equal(mr->range[0].max_ip, addr.s_addr);
    assert_int_equal(mr->range[0].flags & IP_NAT_RANGE_PROTO_SPECIFIED, IP_NAT_RANGE_PROTO_SPECIFIED);
    assert_int_equal(ntohs(mr->range[0].min.tcp.port), 80);
    assert_int_equal(ntohs(mr->range[0].max.tcp.port), 80);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule13(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct xt_DSCP_info* dscp_info = NULL;

    //Set DSCP to EF (0x2e, 46).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_target_dscp(rule, 0x2e);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DSCP");
    dscp_info = (struct xt_DSCP_info*) element->elem;
    assert_non_null(dscp_info);
    assert_int_equal(dscp_info->dscp, fw_rule_get_target_dscp_option(rule));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule14(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* m = NULL;
    struct ipt_icmp* info = NULL;

    // ICMP network-unknown rule (3, 6).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_protocol(rule, 1);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_icmp_type(rule, 3);
    retval |= fw_rule_set_icmp_code(rule, 6);
    retval |= fw_rule_set_source(rule, "192.168.1.20");
    retval |= fw_rule_set_destination(rule, "192.168.1.10");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);


    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "icmp");
    info = (struct ipt_icmp*) m->data;
    assert_int_equal(info->type, 3);
    assert_int_equal(info->code[0], 6);
    assert_int_equal(info->code[1], 6);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule15(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* m = NULL;
    struct ipt_icmp* info = NULL;

    // ICMP redirect rule (5, -1).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_protocol(rule, 1);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_icmp_type(rule, 5);
    retval |= fw_rule_set_icmp_code(rule, -1);
    retval |= fw_rule_set_source(rule, "192.168.1.20");
    retval |= fw_rule_set_destination(rule, "192.168.1.10");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "icmp");
    info = (struct ipt_icmp*) m->data;
    assert_int_equal(info->type, 5);
    assert_int_equal(info->code[0], 0);
    assert_int_equal(info->code[1], 0xFF);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule16(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* match = NULL;
    struct xt_physdev_info* info = NULL;

    //Set physdev-in match to !eth1, physdev-out match to eth2, target DSCP 0x28.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_physdev_in(rule, "eth1");
    retval |= fw_rule_set_physdev_in_excluded(rule, true);
    retval |= fw_rule_set_physdev_out(rule, "eth2");
    retval |= fw_rule_set_physdev_out_excluded(rule, false);
    retval |= fw_rule_set_target_dscp(rule, 0x28);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 2);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    match = (struct xt_entry_match*) element->elem;
    assert_non_null(match);
    assert_string_equal(match->u.user.name, "physdev");

    info = (struct xt_physdev_info*) match->data;
    assert_non_null(info);
    assert_string_equal(info->physoutdev, fw_rule_get_physdev_out(rule));
    assert_false(info->invert);

    match_it = amxc_llist_it_get_next(match_it);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    match = (struct xt_entry_match*) element->elem;
    assert_non_null(match);
    assert_string_equal(match->u.user.name, "physdev");

    info = (struct xt_physdev_info*) match->data;
    assert_non_null(info);
    assert_string_equal(info->physindev, fw_rule_get_physdev_in(rule));
    assert_true(info->invert);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule17(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_set_info* set_info = NULL;
    struct xt_entry_match* m;
    const char* source_set_name = "src-set";
    const int source_set_index = 5;
    const char* dest_set_name = "dst-set";
    const int dest_set_index = 9;

    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_source_ipset(rule, source_set_name);
    retval |= fw_rule_set_destination_ipset(rule, dest_set_name);
    retval |= fw_rule_set_destination_ipset_excluded(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    will_return(__wrap_getsockopt, source_set_index);
    expect_string(__wrap_getsockopt, req->set.name, source_set_name);
    will_return(__wrap_getsockopt, dest_set_index);
    expect_string(__wrap_getsockopt, req->set.name, dest_set_name);
    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 2);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "set");
    set_info = (struct xt_set_info*) m->data;
    assert_non_null(set_info);
    assert_int_equal(set_info->index, source_set_index);
    assert_int_equal(set_info->dim, IPSET_DIM_ONE);
    assert_int_equal(set_info->flags, IPSET_DIM_ONE_SRC);

    match_it = amxc_llist_get_last(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "set");
    set_info = (struct xt_set_info*) m->data;
    assert_non_null(set_info);
    assert_int_equal(set_info->index, dest_set_index);
    assert_int_equal(set_info->dim, IPSET_DIM_ONE);
    assert_int_equal(set_info->flags, IPSET_INV_MATCH);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rule18(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_set_info* set_info = NULL;
    struct xt_entry_match* m;
    const char* source_set_name = "src-set";
    const int source_set_index = 5;
    const char* dest_set_name = "dst-set";
    const int dest_set_index = 9;

    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_source_ipset(rule, source_set_name);
    retval |= fw_rule_set_source_ipset_excluded(rule, true);
    retval |= fw_rule_set_destination_ipset(rule, dest_set_name);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    will_return(__wrap_getsockopt, source_set_index);
    expect_string(__wrap_getsockopt, req->set.name, source_set_name);
    will_return(__wrap_getsockopt, dest_set_index);
    expect_string(__wrap_getsockopt, req->set.name, dest_set_name);
    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 2);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "set");
    set_info = (struct xt_set_info*) m->data;
    assert_non_null(set_info);
    assert_int_equal(set_info->index, source_set_index);
    assert_int_equal(set_info->dim, IPSET_DIM_ONE);
    assert_int_equal(set_info->flags, IPSET_DIM_ONE_SRC | IPSET_INV_MATCH);

    match_it = amxc_llist_get_last(&r->match_list);
    assert_non_null(match_it);
    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "set");
    set_info = (struct xt_set_info*) m->data;
    assert_non_null(set_info);
    assert_int_equal(set_info->index, dest_set_index);
    assert_int_equal(set_info->dim, IPSET_DIM_ONE);
    assert_int_equal(set_info->flags, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}


static void test_interface_iptc_create_ipv6_rule1(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    struct xt_standard_target* target = NULL;
    struct in6_addr addr6 = {0};
    char str_addr6[INET6_ADDRSTRLEN] = {0};

    //Block all forwarded traffic on interface wlan0 from 12AB:0:0:CD30:123:4567:89AB:CDEF.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_in_interface(rule, "wlan0");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567:89ab:cdef");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry6->ipv6.iniface, fw_rule_get_in_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_in_interface(rule)) + 1);
    assert_memory_equal(r->e.entry6->ipv6.iniface_mask, intf_mask,
                        strlen(fw_rule_get_in_interface(rule)) + 1);

    assert_int_equal(inet_pton(AF_INET6, fw_rule_get_source(rule), &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.src, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.smsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_VIA_IN, 0);
    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_SRCIP, 0);

    target = (struct xt_standard_target*) r->t.target;
    assert_non_null(target);
    assert_int_equal(target->verdict, -NF_DROP - 1);
    assert_string_equal(r->t.target->u.user.name, "DROP");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Alter rule: exclude interface and source.
    retval |= fw_rule_set_in_interface_excluded(rule, true);
    retval |= fw_rule_set_source_excluded(rule, true);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_VIA_IN, IP6T_INV_VIA_IN);
    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_SRCIP, IP6T_INV_SRCIP);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Alter rule: set different prefixes
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567:89ab:cdef/60");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30::", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.src, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.smsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:fff0::");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //source prefix too big.
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567:89ab:cdef/148");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30:123:4567:89ab:cdef", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.src, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.smsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //source: use colons.
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567::");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30:123:4567:0000:0000", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.src, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.smsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Invalid source 1
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567/18");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //Invalid source 2
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:::4567");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //source mask: use colons.
    retval |= fw_rule_set_source(rule, "12ab:0:0:cd30:123:4567:89ab:cdef");
    retval |= fw_rule_set_source_mask(rule, "ffff:ffff:ffff:fff0::");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30::", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.src, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.smsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:fff0::");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule2(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    unsigned char intf_mask[IFNAMSIZ] = {0};
    struct xt_standard_target* target = NULL;
    struct in6_addr addr6 = {0};
    char str_addr6[INET6_ADDRSTRLEN] = {0};

    //Accept all outgoing traffic on interface ptm0 to 12AB:0:0:CD30:123:4567:89AB:CDEF.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "OUTPUT");
    retval |= fw_rule_set_out_interface(rule, "ptm0");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567:89ab:cdef");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_string_equal(r->e.entry6->ipv6.outiface, fw_rule_get_out_interface(rule));
    memset(intf_mask, 0xFF, strlen(fw_rule_get_out_interface(rule)) + 1);
    assert_memory_equal(r->e.entry6->ipv6.outiface_mask, intf_mask,
                        strlen(fw_rule_get_out_interface(rule)) + 1);

    assert_int_equal(inet_pton(AF_INET6, fw_rule_get_destination(rule), &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.dst, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.dmsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_VIA_OUT, 0);
    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_DSTIP, 0);

    target = (struct xt_standard_target*) r->t.target;
    assert_non_null(target);
    assert_int_equal(target->verdict, -NF_ACCEPT - 1);
    assert_string_equal(r->t.target->u.user.name, "ACCEPT");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Alter rule: exclude outgoing interface and destination.
    retval |= fw_rule_set_out_interface_excluded(rule, true);
    retval |= fw_rule_set_destination_excluded(rule, true);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_VIA_OUT, IP6T_INV_VIA_OUT);
    assert_int_equal(r->e.entry6->ipv6.invflags & IP6T_INV_DSTIP, IP6T_INV_DSTIP);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Alter rule: set different prefixes
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567:89ab:cdef/60");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30::", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.dst, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.dmsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:fff0::");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //source prefix too big.
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567:89ab:cdef/148");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30:123:4567:89ab:cdef", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.dst, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.dmsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //source: use colons.
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567::");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30:123:4567:0000:0000", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.dst, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.dmsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Invalid destination 1
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567/18");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //Invalid destination 2
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:::4567");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //destination mask: use colons.
    retval |= fw_rule_set_destination(rule, "12ab:0:0:cd30:123:4567:89ab:cdef");
    retval |= fw_rule_set_destination_mask(rule, "ffff:ffff:ffff:fff0::");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(inet_pton(AF_INET6, "12ab:0:0:cd30::", &addr6), 1);
    assert_memory_equal(&r->e.entry6->ipv6.dst, &addr6, sizeof(addr6));

    assert_non_null(inet_ntop(AF_INET6, &r->e.entry6->ipv6.dmsk, str_addr6, sizeof(str_addr6)));
    assert_string_equal(str_addr6, "ffff:ffff:ffff:fff0::");

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule3(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Accept all TCP traffic to hosts.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    retval |= fw_rule_set_protocol(rule, 6);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry6->ipv6.invflags & IPT_INV_PROTO, 0);
    assert_int_equal(r->e.entry6->ipv6.proto, fw_rule_get_protocol(rule));
    assert_int_equal(r->e.entry6->ipv6.flags & 0x1, 1);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    //Exclude UDP.
    retval |= fw_rule_set_protocol(rule, 17);
    retval |= fw_rule_set_protocol_excluded(rule, true);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(r->e.entry6->ipv6.invflags & IPT_INV_PROTO, IPT_INV_PROTO);
    assert_int_equal(r->e.entry6->ipv6.proto, fw_rule_get_protocol(rule));
    assert_int_equal(r->e.entry6->ipv6.flags & 0x1, 1);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule4(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_mac_info* mac_info = NULL;
    struct xt_entry_match* entry_match = NULL;

    //Accept all traffic to CPE from host a1:d2:5a:fe:bc:1b
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    retval |= fw_rule_set_source_mac_address(rule, "a0:d3:7a:fa:fc:1c");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    assert_string_equal(entry_match->u.user.name, "mac");
    mac_info = (struct xt_mac_info*) entry_match->data;
    assert_non_null(mac_info);
    assert_int_equal(mac_info->srcaddr[0], (int) strtol("a0", NULL, 16));
    assert_int_equal(mac_info->srcaddr[1], (int) strtol("d3", NULL, 16));
    assert_int_equal(mac_info->srcaddr[2], (int) strtol("7a", NULL, 16));
    assert_int_equal(mac_info->srcaddr[3], (int) strtol("fa", NULL, 16));
    assert_int_equal(mac_info->srcaddr[4], (int) strtol("fc", NULL, 16));
    assert_int_equal(mac_info->srcaddr[5], (int) strtol("1c", NULL, 16));
    assert_int_equal(mac_info->invert, 0);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule5(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct xt_mark_tginfo2* mark_info = NULL;
    uint32_t target_mark = 0;
    uint32_t target_mask = 0;

    //Mark packets with 0xabcd.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_mark(rule, 0xabcd, 5);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "MARK");
    assert_int_equal(r->t.target->u.user.revision, 2);
    mark_info = (struct xt_mark_tginfo2*) element->elem;
    assert_non_null(mark_info);
    fw_rule_get_target_mark_options(rule, &target_mark, &target_mask);
    assert_int_equal(mark_info->mark, target_mark);
    assert_int_equal(mark_info->mask, target_mask);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule6(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct xt_DSCP_info* dscp_info = NULL;

    //Set DSCP to EF (0x2e, 46).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_dscp(rule, 0x2e);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "DSCP");
    dscp_info = (struct xt_DSCP_info*) element->elem;
    assert_non_null(dscp_info);
    assert_int_equal(dscp_info->dscp, fw_rule_get_target_dscp_option(rule));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule7(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Set queue (not supported yet).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_queue(rule, 1);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    //Expand test when supported.

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule8(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Masquerade
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "NAT");
    retval |= fw_rule_set_chain(rule, "POSTROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_MASQUERADE);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    //Expand test when TODO is done in the library.

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule9(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;

    //Redirect TCP traffic to destination port 32500 to port 80.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_protocol(rule, 6);
    //retval |= fw_rule_set_target_redirect(rule, "PREROUTING_class");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_null(r);

    //TODO: expand test when redirect is added in lib_fwrules.

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule10(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* target_it = NULL;
    struct element* element = NULL;
    struct xt_classify_target_info* class_info = NULL;
    uint32_t target_class = 0;

    //Classify packets with class = 1.
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_classify(rule, 1);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->target_list), 1);
    target_it = amxc_llist_get_first(&r->target_list);
    assert_non_null(target_it);

    element = amxc_container_of(target_it, struct element, it);
    assert_non_null(element);
    assert_string_equal(r->t.target->u.user.name, "CLASSIFY");
    class_info = (struct xt_classify_target_info*) element->elem;
    assert_non_null(class_info);
    fw_rule_get_target_classify_option(rule, &target_class);
    assert_int_equal(class_info->priority, target_class);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule11(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* m = NULL;
    struct ip6t_icmp* info = NULL;

    // ICMPv6 beyond-scope rule (1, 2).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_protocol(rule, 58);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_icmpv6_type(rule, 1);
    retval |= fw_rule_set_icmpv6_code(rule, 2);
    retval |= fw_rule_set_source(rule, "fd12:3456:789a:1::/64");
    retval |= fw_rule_set_destination(rule, "fd12:3456:789a:2::/64");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "icmp6");
    info = (struct ip6t_icmp*) m->data;
    assert_int_equal(info->type, 1);
    assert_int_equal(info->code[0], 2);
    assert_int_equal(info->code[1], 2);

    iptc_rule_delete(r, fw_rule_get_ipv6(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule12(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* m = NULL;
    struct ip6t_icmp* info = NULL;

    // ICMPv6 parameter-problem rule (4, -1).
    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_protocol(rule, 58);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_icmpv6_type(rule, 4);
    retval |= fw_rule_set_icmpv6_code(rule, -1);
    retval |= fw_rule_set_source(rule, "fd12:3456:789a:1::/64");
    retval |= fw_rule_set_destination(rule, "fd12:3456:789a:2::/64");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    m = (struct xt_entry_match*) element->elem;
    assert_non_null(m);
    assert_string_equal(m->u.user.name, "icmp6");
    info = (struct ip6t_icmp*) m->data;
    assert_int_equal(info->type, 4);
    assert_int_equal(info->code[0], 0);
    assert_int_equal(info->code[1], 0xFF);

    iptc_rule_delete(r, fw_rule_get_ipv6(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule13(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* entry_match = NULL;
    struct xt_string_info* string_info = NULL;

    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    retval |= fw_rule_set_filter_string(rule, "GET /index.html");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    assert_string_equal(entry_match->u.user.name, "string");
    string_info = (struct xt_string_info*) entry_match->data;
    assert_non_null(string_info);
    assert_string_equal(string_info->pattern, fw_rule_get_filter_string(rule));

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv6_rule14(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    struct iptc_rule* r = NULL;
    amxc_llist_it_t* match_it = NULL;
    struct element* element = NULL;
    struct xt_entry_match* entry_match = NULL;
    struct xt_time_info* time_info = NULL;

    retval |= fw_rule_new(&rule);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    retval |= fw_rule_set_filter_weekdays(rule, "Mon,Tue,Wed,Thu,Fri,Sat,Sun");
    retval |= fw_rule_set_filter_start_time(rule, "00:00:01");
    retval |= fw_rule_set_filter_end_time(rule, "23:59:59");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    assert_string_equal(entry_match->u.user.name, "time");
    time_info = (struct xt_time_info*) entry_match->data;
    assert_non_null(time_info);
    assert_int_equal(time_info->flags, XT_TIME_LOCAL_TZ);
    assert_int_equal(time_info->weekdays_match, 0xfe);
    assert_int_equal(time_info->daytime_start, 0x1);
    assert_int_equal(time_info->daytime_stop, 0x1517f);
    assert_int_equal(time_info->monthdays_match, XT_TIME_ALL_MONTHDAYS);
    assert_int_equal(time_info->date_start, 0);
    assert_int_equal(time_info->date_stop, INT_MAX);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    // condition starttime > endtime sets flag XT_TIME_CONTIGUOUS
    retval |= fw_rule_set_filter_start_time(rule, "23:00:00");
    retval |= fw_rule_set_filter_end_time(rule, "01:00:00");
    assert_int_equal(retval, 0);

    r = iptc_create_rule(rule);
    assert_non_null(r);

    assert_int_equal(amxc_llist_size(&r->match_list), 1);
    match_it = amxc_llist_get_first(&r->match_list);
    assert_non_null(match_it);

    element = amxc_container_of(match_it, struct element, it);
    assert_non_null(element);
    entry_match = (struct xt_entry_match*) element->elem;
    assert_non_null(entry_match);
    assert_string_equal(entry_match->u.user.name, "time");
    time_info = (struct xt_time_info*) entry_match->data;
    assert_non_null(time_info);
    assert_int_equal(time_info->flags, XT_TIME_LOCAL_TZ | XT_TIME_CONTIGUOUS);
    assert_int_equal(time_info->weekdays_match, 0xfe);
    assert_int_equal(time_info->daytime_start, 82800);
    assert_int_equal(time_info->daytime_stop, 3600);
    assert_int_equal(time_info->monthdays_match, XT_TIME_ALL_MONTHDAYS);
    assert_int_equal(time_info->date_start, 0);
    assert_int_equal(time_info->date_stop, INT_MAX);

    iptc_rule_delete(r, fw_rule_get_ipv4(rule));

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
    assert_null(rule);
}

static void test_interface_iptc_create_ipv4_rules(void) {
    test_interface_iptc_create_ipv4_rule1();
    test_interface_iptc_create_ipv4_rule2();
    test_interface_iptc_create_ipv4_rule3();
    test_interface_iptc_create_ipv4_rule4();
    test_interface_iptc_create_ipv4_rule5();
    test_interface_iptc_create_ipv4_rule6();
    test_interface_iptc_create_ipv4_rule7();
    test_interface_iptc_create_ipv4_rule8();
    test_interface_iptc_create_ipv4_rule9();
    test_interface_iptc_create_ipv4_rule10();
    test_interface_iptc_create_ipv4_rule11();
    test_interface_iptc_create_ipv4_rule12();
    test_interface_iptc_create_ipv4_rule13();
    test_interface_iptc_create_ipv4_rule14();
    test_interface_iptc_create_ipv4_rule15();
    test_interface_iptc_create_ipv4_rule16();
    test_interface_iptc_create_ipv4_rule17();
    test_interface_iptc_create_ipv4_rule18();
}

static void test_interface_iptc_create_ipv6_rules(void) {
    test_interface_iptc_create_ipv6_rule1();
    test_interface_iptc_create_ipv6_rule2();
    test_interface_iptc_create_ipv6_rule3();
    test_interface_iptc_create_ipv6_rule4();
    test_interface_iptc_create_ipv6_rule5();
    test_interface_iptc_create_ipv6_rule6();
    test_interface_iptc_create_ipv6_rule7();
    test_interface_iptc_create_ipv6_rule8();
    test_interface_iptc_create_ipv6_rule9();
    test_interface_iptc_create_ipv6_rule10();
    test_interface_iptc_create_ipv6_rule11();
    test_interface_iptc_create_ipv6_rule12();
    test_interface_iptc_create_ipv6_rule13();
    test_interface_iptc_create_ipv6_rule14();
}

void test_interface_iptc_create_rule(UNUSED void** state) {
    test_interface_iptc_create_ipv4_rules();
    test_interface_iptc_create_ipv6_rules();
}


static void test_interface_iptc_append_existing_ipv4_rule(void) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = "mangle";
    const char* chain = "POSTROUTING";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);

    retval |= fw_rule_set_table(rule, table);
    retval |= fw_rule_set_chain(rule, chain);
    retval |= fw_rule_set_target_chain(rule, "POSTROUTING_two");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    //Rule doesn't exist.
    expect_string(__wrap_iptc_init, table, table);
    expect_string(__wrap_iptc_append_entry, chain, chain);
    will_return(__wrap_iptc_append_entry, 1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, 0);

    //Rule exists.
    expect_string(__wrap_iptc_append_entry, chain, chain);
    will_return(__wrap_iptc_append_entry, 1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, 0);

    retval = fw_apply();
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}

static void test_interface_iptc_append_existing_ipv6_rule(void) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = "mangle";
    const char* chain = "POSTROUTING";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);

    retval |= fw_rule_set_table(rule, table);
    retval |= fw_rule_set_chain(rule, chain);
    retval |= fw_rule_set_target_chain(rule, "POSTROUTING_two");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    //Rule doesn't exist.
    expect_string(__wrap_ip6tc_init, table, table);
    expect_string(__wrap_ip6tc_append_entry, chain, chain);
    will_return(__wrap_ip6tc_append_entry, 1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, 0);

    //Rule exists.
    expect_string(__wrap_ip6tc_append_entry, chain, chain);
    will_return(__wrap_ip6tc_append_entry, 1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, 0);

    retval = fw_apply();
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}

static void test_interface_iptc_insert_existing_ipv4_rule(void) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = "mangle";
    const char* chain = "POSTROUTING";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);

    retval |= fw_rule_set_table(rule, table);
    retval |= fw_rule_set_chain(rule, chain);
    retval |= fw_rule_set_target_chain(rule, "POSTROUTING_two");
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    //Rule doesn't exist.
    expect_string(__wrap_iptc_init, table, table);
    expect_string(__wrap_iptc_insert_entry, chain, chain);
    will_return(__wrap_iptc_insert_entry, 1);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, 0);

    //Rule exists.
    expect_string(__wrap_iptc_insert_entry, chain, chain);
    will_return(__wrap_iptc_insert_entry, 1);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, 0);

    retval = fw_apply();
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}

static void test_interface_iptc_insert_existing_ipv6_rule(void) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = "mangle";
    const char* chain = "POSTROUTING";

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);

    retval |= fw_rule_set_table(rule, table);
    retval |= fw_rule_set_chain(rule, chain);
    retval |= fw_rule_set_target_chain(rule, "POSTROUTING_two");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    //Rule doesn't exist.
    expect_string(__wrap_ip6tc_init, table, table);
    expect_string(__wrap_ip6tc_insert_entry, chain, chain);
    will_return(__wrap_ip6tc_insert_entry, 1);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, 0);

    //Rule exists.
    expect_string(__wrap_ip6tc_insert_entry, chain, chain);
    will_return(__wrap_ip6tc_insert_entry, 1);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, 0);

    retval = fw_apply();
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}

void test_interface_iptc_create_existing_rule(UNUSED void** state) {
    will_return_always(__wrap_iptc_commit, 1);
    will_return_always(__wrap_ip6tc_commit, 1);
    will_return_always(__wrap_file_flock, 0);

    test_interface_iptc_append_existing_ipv4_rule();
    test_interface_iptc_append_existing_ipv6_rule();
    test_interface_iptc_insert_existing_ipv4_rule();
    test_interface_iptc_insert_existing_ipv6_rule();
}

void test_interface_iptc_flock_fails(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* table = "filter";
    const char* chain = "INPUT";

    will_return_always(__wrap_file_flock, -1);

    retval = fw_add_chain(chain, table, true);
    assert_int_equal(retval, -1);

    retval = fw_add_chain(chain, table, false);
    assert_int_equal(retval, -1);

    retval = fw_rule_new(&rule);
    assert_int_equal(retval, 0);

    retval |= fw_rule_set_table(rule, table);
    retval |= fw_rule_set_chain(rule, chain);
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_enabled(rule, true);
    assert_int_equal(retval, 0);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, -1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, -1);

    retval = fw_rule_set_ipv4(rule, false);
    assert_int_equal(retval, 0);

    retval = fw_replace_rule(rule, 2);
    assert_int_equal(retval, -1);

    retval = fw_append_rule(rule);
    assert_int_equal(retval, -1);

    retval = fw_delete_rule(rule, 2);
    assert_int_equal(retval, -1);

    retval = fw_apply();
    assert_int_equal(retval, 0);

    retval = fw_rule_delete(&rule);
    assert_int_equal(retval, 0);
}