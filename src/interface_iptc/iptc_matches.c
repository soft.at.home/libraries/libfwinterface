/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interface_iptc/iptc_matches.h"
#include "fw_interface.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(__GLIBC__) && __GLIBC__ == 2
#include <net/ethernet.h>
#else
#include <linux/if_ether.h>
#endif
#include <linux/if.h>
#include <linux/version.h>
#include <linux/netfilter/xt_tcpudp.h>
#include <linux/netfilter/x_tables.h>
#include <linux/netfilter/xt_length.h>
#include <linux/netfilter/xt_dscp.h>
#include <linux/netfilter/xt_mac.h>
#include <linux/netfilter/xt_conntrack.h>
#include <linux/netfilter/xt_mark.h>
#include <linux/netfilter/xt_connmark.h>
#include <linux/netfilter/xt_connbytes.h>
#include <linux/netfilter/xt_string.h>
#include <linux/netfilter/xt_time.h>
#include <linux/netfilter/xt_set.h>
#include <linux/netfilter/nf_conntrack_common.h>
#include <linux/netfilter_ipv4/ip_tables.h>
#include <linux/netfilter_ipv6/ip6_tables.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>

#include <amxc/amxc_macros.h>

#define typeof __typeof__ // for XT_ALIGN

struct xt_entry_match* iptc_rule_add_mac_match(struct iptc_rule* r, uint8_t* smac, int sinvert) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int mac_size = XT_ALIGN(sizeof(struct xt_mac_info));
    struct xt_entry_match* m = NULL;
    struct xt_mac_info* mac_info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + mac_size);
    when_null(m, out);
    mac_info = (struct xt_mac_info*) m->data;

    m->u.match_size = size + mac_size;
    strncpy(m->u.user.name, "mac", XT_EXTENSION_MAXNAMELEN);
    memcpy(mac_info->srcaddr, smac, ETH_ALEN);
    mac_info->invert = sinvert;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + mac_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_string_match(struct iptc_rule* r, const char* string) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int str_size = XT_ALIGN(sizeof(struct xt_string_info));
    struct xt_entry_match* m = NULL;
    struct xt_string_info* string_info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + str_size);
    when_null(m, out);
    string_info = (struct xt_string_info*) m->data;

    m->u.match_size = size + str_size;
    m->u.user.revision = 1;
    strncpy(m->u.user.name, "string", XT_EXTENSION_MAXNAMELEN);

    strncpy(string_info->algo, "bm", XT_STRING_MAX_ALGO_NAME_SIZE);
    if(strlen(string) <= XT_STRING_MAX_PATTERN_SIZE) {
        strncpy(string_info->pattern, string, XT_STRING_MAX_PATTERN_SIZE);
        string_info->patlen = strnlen(string, XT_STRING_MAX_PATTERN_SIZE);
        string_info->to_offset = UINT16_MAX;
    }

    string_info->u.v1.flags = XT_STRING_FLAG_IGNORECASE;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + str_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_time_match(struct iptc_rule* r, uint32_t time_start, uint32_t time_stop, uint8_t weekdays) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int time_size = XT_ALIGN(sizeof(struct xt_time_info));
    struct xt_entry_match* m = NULL;
    struct xt_time_info* time_info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + time_size);
    when_null(m, out);
    time_info = (struct xt_time_info*) m->data;

    m->u.match_size = size + time_size;
    strncpy(m->u.user.name, "time", XT_EXTENSION_MAXNAMELEN);
    time_info->daytime_start = time_start;
    time_info->daytime_stop = time_stop;
    time_info->weekdays_match = weekdays;
    time_info->flags = XT_TIME_LOCAL_TZ;
    time_info->monthdays_match = XT_TIME_ALL_MONTHDAYS;
    time_info->date_start = 0;
    time_info->date_stop = INT_MAX;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 7, 0)
    if(time_start > time_stop) {
        time_info->flags |= XT_TIME_CONTIGUOUS;
    }
#endif

    iptc_rule_add_match_element(r, (unsigned char*) m, size + time_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_tcp_match(struct iptc_rule* r,
                                               int dport, int dport_max, int dflags,
                                               int sport, int sport_max, int sflags) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int tcp_size = XT_ALIGN(sizeof(struct xt_tcp));
    struct xt_entry_match* m = NULL;
    struct xt_tcp* tcp_info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + tcp_size);
    when_null(m, out);
    tcp_info = (struct xt_tcp*) m->data;

    m->u.match_size = size + tcp_size;
    strncpy(m->u.user.name, "tcp", XT_EXTENSION_MAXNAMELEN);
    if(dport > 0) {
        tcp_info->dpts[0] = dport;
        tcp_info->dpts[1] = (dport_max > dport) ? dport_max : dport;
        tcp_info->invflags |= dflags;
    } else {
        tcp_info->dpts[1] = 0xffff;
    }
    if(sport > 0) {
        tcp_info->spts[0] = sport;
        tcp_info->spts[1] = (sport_max > sport) ? sport_max : sport;
        tcp_info->invflags |= sflags;
    } else {
        tcp_info->spts[1] = 0xffff;
    }

    iptc_rule_add_match_element(r, (unsigned char*) m, size + tcp_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_udp_match(struct iptc_rule* r,
                                               int dport, int dport_max, int dflags,
                                               int sport, int sport_max, int sflags) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int udp_size = XT_ALIGN(sizeof(struct xt_udp));
    struct xt_entry_match* m = NULL;
    struct xt_udp* udp_info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + udp_size);
    when_null(m, out);
    udp_info = (struct xt_udp*) m->data;

    m->u.match_size = size + udp_size;
    strncpy(m->u.user.name, "udp", XT_EXTENSION_MAXNAMELEN);
    if(dport > 0) {
        udp_info->dpts[0] = dport;
        udp_info->dpts[1] = (dport_max > dport) ? dport_max : dport;
        udp_info->invflags |= dflags;
    } else {
        udp_info->dpts[1] = 0xffff;
    }
    if(sport > 0) {
        udp_info->spts[0] = sport;
        udp_info->spts[1] = (sport_max > sport) ? sport_max : sport;
        udp_info->invflags |= sflags;
    } else {
        udp_info->spts[1] = 0xffff;
    }

    iptc_rule_add_match_element(r, (unsigned char*) m, size + udp_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_icmp_match(struct iptc_rule* r, unsigned char type, unsigned char code, unsigned char flags) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int icmp_size = XT_ALIGN(sizeof(struct ipt_icmp));
    struct xt_entry_match* m = NULL;
    struct ipt_icmp* info = NULL;

    when_true((type == 0), out);
    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + icmp_size);
    when_null(m, out);
    info = (struct ipt_icmp*) m->data;

    m->u.match_size = size + icmp_size;
    strncpy(m->u.user.name, "icmp", XT_EXTENSION_MAXNAMELEN);
    info->invflags = flags;
    info->type = type;
    // When the code is not used, the value passed in parameter is (unsigned char)-1 => 255
    if(code == 255) {
        info->code[1] = 0xFF;
    } else {
        info->code[0] = code;
        info->code[1] = code;
    }

    iptc_rule_add_match_element(r, (unsigned char*) m, size + icmp_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_icmp6_match(struct iptc_rule* r, unsigned char type, unsigned char code, unsigned char flags) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int icmp_size = XT_ALIGN(sizeof(struct ip6t_icmp));
    struct xt_entry_match* m = NULL;
    struct ip6t_icmp* info = NULL;

    when_true((type == 0), out);
    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + icmp_size);
    when_null(m, out);
    info = (struct ip6t_icmp*) m->data;

    m->u.match_size = size + icmp_size;
    strncpy(m->u.user.name, "icmp6", XT_EXTENSION_MAXNAMELEN);
    info->invflags = flags;
    info->type = type;
    // When the code is not used, the value passed in parameter is (unsigned char)-1 => 255
    if(code == 255) {
        info->code[1] = 0xFF;
    } else {
        info->code[0] = code;
        info->code[1] = code;
    }

    iptc_rule_add_match_element(r, (unsigned char*) m, size + icmp_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_length_match(struct iptc_rule* r, unsigned short min, unsigned short max, unsigned char exclude) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int len_size = XT_ALIGN(sizeof(struct xt_length_info));
    struct xt_entry_match* m = NULL;
    struct xt_length_info* info = NULL;

    when_null(r, out);
    when_true((min == 0), out);

    m = (struct xt_entry_match*) calloc(1, size + len_size);
    when_null(m, out);

    info = (struct xt_length_info*) m->data;

    m->u.match_size = size + len_size;
    strncpy(m->u.user.name, "length", XT_EXTENSION_MAXNAMELEN);

    info->min = min;
    info->max = (max >= min) ? max : 0xffff;
    info->invert = exclude;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + len_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_dscp_match(struct iptc_rule* r, unsigned char dscp, unsigned char flag) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int dscp_size = XT_ALIGN(sizeof(struct xt_length_info));
    struct xt_entry_match* m = NULL;
    struct xt_dscp_info* info = NULL;

    when_null(r, out);
    when_true((dscp == 0), out);

    m = (struct xt_entry_match*) calloc(1, size + dscp_size);
    when_null(m, out);

    info = (struct xt_dscp_info*) m->data;

    m->u.match_size = size + dscp_size;
    strncpy(m->u.user.name, "dscp", XT_EXTENSION_MAXNAMELEN);

    info->dscp = dscp;
    info->invert = flag;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + dscp_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_state_match(struct iptc_rule* r, const char* state) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_conntrack_mtinfo3));
    struct xt_entry_match* m = NULL;
    struct xt_conntrack_mtinfo3* info = NULL;

    when_null(r, out);
    when_str_empty(state, out);

    m = (struct xt_entry_match*) calloc(1, size + info_size);
    when_null(m, out);

    info = (struct xt_conntrack_mtinfo3*) m->data;

    m->u.match_size = size + info_size;
    strncpy(m->u.user.name, "conntrack", XT_EXTENSION_MAXNAMELEN);
    m->u.user.revision = 3;

    if(strstr(state, "INVALID") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_INVALID;
    }
    if(strstr(state, "NEW") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_BIT(IP_CT_NEW);
    }
    if(strstr(state, "RELATED") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_BIT(IP_CT_RELATED);
    }
    if(strstr(state, "ESTABLISHED") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_BIT(IP_CT_ESTABLISHED);
    }
    if(strstr(state, "UNTRACKED") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_UNTRACKED;
    }
    if(strstr(state, "SNAT") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_SNAT;
    }
    if(strstr(state, "DNAT") != NULL) {
        info->state_mask |= XT_CONNTRACK_STATE_DNAT;
    }

    info->match_flags = XT_CONNTRACK_STATE;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + info_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_connbytes_match(struct iptc_rule* r, unsigned long from, unsigned long to, const char* what, const char* direction) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_connbytes_info));
    struct xt_entry_match* m = NULL;
    struct xt_connbytes_info* info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + info_size);
    when_null(m, out);

    info = (struct xt_connbytes_info*) m->data;

    m->u.match_size = size + info_size;
    strncpy(m->u.user.name, "connbytes", XT_EXTENSION_MAXNAMELEN);

    info->count.from = from;
    info->count.to = to;

    if(strcmp(what, "packets") == 0) {
        info->what = XT_CONNBYTES_PKTS;
    } else if(strcmp(what, "bytes") == 0) {
        info->what = XT_CONNBYTES_BYTES;
    } else if(strcmp(what, "averagepacket") == 0) {
        info->what = XT_CONNBYTES_AVGPKT;
    }

    if(strcmp(direction, "original") == 0) {
        info->direction = XT_CONNBYTES_DIR_ORIGINAL;
    } else if(strcmp(direction, "reply") == 0) {
        info->direction = XT_CONNBYTES_DIR_REPLY;
    } else if(strcmp(direction, "both") == 0) {
        info->direction = XT_CONNBYTES_DIR_BOTH;
    }

    iptc_rule_add_match_element(r, (unsigned char*) m, size + info_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_skbmark_match(struct iptc_rule* r, unsigned int mark, unsigned int mask, unsigned char exclude) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_mark_mtinfo1));
    struct xt_entry_match* m = NULL;
    struct xt_mark_mtinfo1* info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + info_size);
    when_null(m, out);

    info = (struct xt_mark_mtinfo1*) m->data;

    m->u.match_size = size + info_size;
    strncpy(m->u.user.name, "mark", XT_EXTENSION_MAXNAMELEN);
    m->u.user.revision = 1;

    info->mark = mark;
    info->mask = mask;
    info->invert = exclude;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + info_size);
out:
    return m;
}

struct xt_entry_match* iptc_rule_add_connmark_match(struct iptc_rule* r, unsigned int mark, unsigned int mask, unsigned char exclude) {
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_connmark_mtinfo1));
    struct xt_entry_match* m = NULL;
    struct xt_connmark_mtinfo1* info = NULL;

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + info_size);
    when_null(m, out);

    info = (struct xt_connmark_mtinfo1*) m->data;

    m->u.match_size = size + info_size;
    strncpy(m->u.user.name, "connmark", XT_EXTENSION_MAXNAMELEN);
    m->u.user.revision = 1;

    info->mark = mark;
    info->mask = mask;
    info->invert = exclude;

    iptc_rule_add_match_element(r, (unsigned char*) m, size + info_size);
out:
    return m;
}

// Inspired from libxt_set.h in iptables code
static ip_set_id_t get_ipset_id_byname(const char* setname) {
    struct ip_set_req_get_set req = {0};
    struct ip_set_req_version req_version = {0};
    socklen_t req_size = sizeof(struct ip_set_req_get_set);
    socklen_t req_version_size = sizeof(struct ip_set_req_version);
    ip_set_id_t set_id = IPSET_INVALID_ID;
    int res = -1;
    int sockfd = -1;

    sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    when_true_trace(sockfd < 0, exit, ERROR, "Can't open socket to ipset");

    res = fcntl(sockfd, F_SETFD, FD_CLOEXEC);
    when_true_trace(res < 0, exit, ERROR, "Could not set close on exec: %s\n", strerror(errno));

    req_version.op = IP_SET_OP_VERSION;
    res = getsockopt(sockfd, SOL_IP, SO_IP_SET, &req_version, &req_version_size);
    when_true_trace(res < 0, exit, ERROR, "Kernel module xt_set is not loaded in");

    SAH_TRACEZ_INFO(ME, "Kernel ipset version %d", req_version.version);

    req.version = req_version.version;
    req.op = IP_SET_OP_GET_BYNAME;
    strncpy(req.set.name, setname, IPSET_MAXNAMELEN);
    req.set.name[IPSET_MAXNAMELEN - 1] = '\0';

    res = getsockopt(sockfd, SOL_IP, SO_IP_SET, &req, &req_size);
    when_true_trace(res < 0, exit, ERROR, "Problem when communicating with ipset, errno=%d", errno);
    when_true_trace(req_size != sizeof(struct ip_set_req_get_set), exit, ERROR, "Incorrect return size from kernel during ipset lookup, (want %zu, got %zu)", sizeof(struct ip_set_req_get_set), (size_t) req_size);
    when_true_trace(req.set.index == IPSET_INVALID_ID, exit, ERROR, "Set %s doesn't exist", setname);

    set_id = req.set.index;
exit:
    close(sockfd);
    return set_id;
}


struct xt_entry_match* iptc_rule_add_set_match(struct iptc_rule* r, const char* setname, bool source, bool exclude) {
    struct xt_entry_match* m = NULL;
    struct xt_set_info* info = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct xt_entry_match));
    int set_id = 0;
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 10, 0)
    // Version v1 available from KERNEL 2.6.39
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_set_info_match_v1));
    int revision = 1;
#elif LINUX_VERSION_CODE < KERNEL_VERSION(3, 19, 0)
    // Version v3 available from KERNEL 3.10.0
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_set_info_match_v3));
    int revision = 3;
#else
    // Version v4 available from KERNEL 3.19.0
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_set_info_match_v4));
    int revision = 4;
#endif

    when_null(r, out);

    m = (struct xt_entry_match*) calloc(1, size + info_size);
    when_null(m, out);

    m->u.match_size = size + info_size;
    m->u.user.revision = revision;
    strncpy(m->u.user.name, "set", XT_EXTENSION_MAXNAMELEN);

    info = (struct xt_set_info*) m->data;
    info->dim = IPSET_DIM_ONE;
    info->flags = source ? IPSET_DIM_ONE_SRC : 0;
    info->flags |= exclude ? IPSET_INV_MATCH : 0;

    set_id = get_ipset_id_byname(setname);
    when_true_trace(set_id == IPSET_INVALID_ID, out, ERROR, "Ignore set match rule");

    info->index = set_id;
    iptc_rule_add_match_element(r, (unsigned char*) m, size + info_size);
out:
    return m;
}