/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/file.h>
#include <errno.h>
#include <linux/version.h>
#ifndef __USE_MISC
#define __USE_MISC
#define __MUST_UNDEFINE_USE_MISC
#endif
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#ifdef __MUST_UNDEFINE_USE_MISC
#undef __USE_MISC
#undef __MUST_UNDEFINE_USE_MISC
#endif

#include <amxc/amxc_macros.h>

#include "fw_interface.h"
#include "interface_iptc/iptc.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw_rule.h>

#include "interface_iptc/iptc_rules.h"
#include "interface_iptc/iptc_matches.h"
#ifdef SUPPORTS_PHYSDEV
#include "interface_iptc/iptc_matches_physdev.h"
#endif
#include "interface_iptc/iptc_targets.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*(x)))
#define STRING_EMPTY(x) (!x || !x[0])

static struct xtc_handle* h_filter = NULL;
static struct xtc_handle* h_nat = NULL;
static struct xtc_handle* h_raw = NULL;
static struct xtc_handle* h_mangle = NULL;
static struct xtc_handle* h6_filter = NULL;
static struct xtc_handle* h6_raw = NULL;
static struct xtc_handle* h6_nat = NULL;
static struct xtc_handle* h6_mangle = NULL;

static bool ipv6_nat_support = true;

static const char* lockfiles[] = {
    NULL, // space for env variable
    "/run/xtables.lock",
    "/var/run/xtables.lock",
};

typedef struct {
    const char* name;
    int fd;
} lockfile_t;

static lockfile_t lockfile = {0};

#define LOCKED (lockfile.fd > 0)

static const char* const week_days[] = {
    NULL, "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun",
};

// keep this in sync with iptables implementation
static bool select_lockfile(void) {
    bool rv = true;
    unsigned int i = 0;

    when_false(STRING_EMPTY(lockfile.name), exit);

    rv = false;

    lockfiles[0] = getenv("XTABLES_LOCKFILE");

    for(i = 0; i < ARRAY_SIZE(lockfiles); i++) {
        if(lockfiles[i] == NULL) {
            continue;
        }
        if(file_access(lockfiles[i]) == 0) {
            lockfile.name = lockfiles[i];
            SAH_TRACEZ_INFO(ME, "Using lock file '%s'", lockfile.name);
            break;
        }
        SAH_TRACEZ_INFO(ME, "Lock file '%s' doesn't exist", lockfiles[i]);
    }

    when_str_empty_trace(lockfile.name, exit, ERROR, "No lockfile found");

    rv = true;

exit:
    return rv;
}

static void reset_lockfile_selection_procedure(void) {
    lockfile.name = NULL;
}

static void unlock(void) {
    if(LOCKED) {
        file_close(lockfile.fd);
        lockfile.fd = -1;
    }
}

static bool lock(void) {
    bool rv = false;

    when_true_status(LOCKED, exit, rv = true);

    when_false(select_lockfile(), exit);

    lockfile.fd = file_open(lockfile.name);
    if(!LOCKED) {
        SAH_TRACEZ_ERROR(ME, "Can't open lock file %s: %s", lockfile.name, strerror(errno));
        reset_lockfile_selection_procedure();
        goto exit;
    }

    if(file_flock(lockfile.fd) != 0) {
        SAH_TRACEZ_ERROR(ME, "flock(%s) failed : %s", lockfile.name, strerror(errno));
        unlock();
        goto exit;
    }

    rv = true;

exit:
    return rv;
}

static unsigned long mask2Long(const char* netmask) {
    unsigned long m = 0;

    if(!netmask || (*netmask == '\0')) {
        m = 0xffffffff;
    } else if(strchr(netmask, '.') != NULL) {
        return (unsigned long) inet_addr(netmask);
    } else {
        int i = atoi(netmask);
        if(i == 0) {
            m = 0;
        } else {
            m = 0xffffffff << (32 - i);
        }
    }
    return htonl(m);
}

static int parsePrefix(const char* prefix, const char* str_mask, in_addr_t* addr, in_addr_t* mask) {
    char* copy = NULL;
    char* netmask = NULL;
    int ret = 0;

    when_str_empty(prefix, out);

    copy = strdup(prefix);
    when_null(copy, out);

    netmask = strstr(copy, "/");
    if(netmask != NULL) {
        netmask[0] = '\0';
        *addr = inet_addr(copy);
        *mask = mask2Long(&netmask[1]);
        *addr &= (unsigned int) *mask;
    } else {
        *addr = inet_addr(prefix);
        *mask = mask2Long(str_mask);
        *addr &= (unsigned int) *mask;
    }
    ret = 1;
out:
    free(copy);
    return ret;
}

static int parseIPv6Mask(const char* str_mask, struct in6_addr* bin_mask) {
    int res = 0;
    const char* colon = NULL;

    when_null(bin_mask, fail);
    when_str_empty(str_mask, fail);

    colon = strstr(str_mask, ":");
    if(colon != NULL) {
        res = inet_pton(AF_INET6, str_mask, bin_mask);
        if(res == 0) {
            SAH_TRACEZ_ERROR(ME, "IPv6 Mask parsing failed[%s]", str_mask);
            return 0;
        }
    } else {
        unsigned int int_mask = atoi(str_mask);
        unsigned int i = 0;
        unsigned int c = 8 - (int_mask % 8);
        if(int_mask > 128) {
            SAH_TRACEZ_NOTICE(ME, "IPV6 integer mask too big[%s].", str_mask);
            goto fail;
        }
        while(int_mask > (i * 8)) {
            if(int_mask < ((i + 1) * 8)) {
                bin_mask->s6_addr[i] = 0xff << c;
                break;
            } else {
                bin_mask->s6_addr[i] = 0xff;
                i++;
            }
        }
    }
    return 1;
fail:
    SAH_TRACEZ_INFO(ME, "No mask provided, assume host ip");
    inet_pton(AF_INET6, "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", bin_mask);
    return 1;
}

static int parsePrefix6(const char* prefix, const char* str_mask, struct in6_addr* addr, struct in6_addr* mask) {
    int res = 0;
    char* copy = NULL;
    char* netmask = NULL;
    int i = 0;

    when_null(prefix, exit);

    copy = strdup(prefix);
    when_null(copy, exit);

    netmask = strstr(copy, "/");
    if(netmask != NULL) {
        netmask[0] = '\0';
        res = inet_pton(AF_INET6, copy, addr);
        if(res == 0) {
            SAH_TRACEZ_ERROR(ME, "IPv6 Address parsing failed[%s]", prefix);
            goto exit;
        }
        parseIPv6Mask(&netmask[1], mask);
    } else {
        res = inet_pton(AF_INET6, prefix, addr);
        if(res == 0) {
            SAH_TRACEZ_ERROR(ME, "IPv6 Address parsing failed[%s]", prefix);
            goto exit;
        }
        parseIPv6Mask(str_mask, mask);
    }

    /*mask IP address with netmask */
    for(i = 0; i < 16; i++) {
        addr->s6_addr[i] &= mask->s6_addr[i];
    }

    res = 1;
exit:
    free(copy);
    return res;
}

static const char* myStrseg(char* buf, unsigned int buflen,
                            const char** arg, char delim) {
    const char* sep = NULL;

    if((*arg == NULL) || (**arg == '\0')) {
        return NULL;
    }
    sep = strchr(*arg, delim);
    if(sep == NULL) {
        snprintf(buf, buflen, "%s", *arg);
        *arg = NULL;
        return buf;
    }
    snprintf(buf, buflen, "%.*s", (unsigned int) (sep - *arg), *arg);
    *arg = sep + 1;
    return buf;
}

static unsigned int timeParseMinutes(const char* s) {
    unsigned int hour = 0;
    unsigned int minute = 0;
    unsigned int second = 0;
    char* endptr = NULL;

    hour = strtoul(s, &endptr, 10);
    if((endptr == NULL) || (*endptr != ':') || (hour > 23)) {
        goto out;
    }

    s = endptr + 1;
    minute = strtoul(s, &endptr, 10);
    if((endptr == NULL) || ((*endptr != ':') && (*endptr != '\0')) || (minute > 59)) {
        goto out;
    }
    if(*endptr == '\0') {
        goto eval;
    }

    s = endptr + 1;
    second = strtoul(s, &endptr, 10);
    if((endptr == NULL) || (*endptr != '\0') || (second > 59)) {
        goto out;
    }

eval:
    return 60 * 60 * hour + 60 * minute + second;

out:
    SAH_TRACEZ_ERROR(ME, "invalid time \"%s\" specified, "
                     "should be hh:mm[:ss] format and within the boundaries", s);
    return -1;
}

static unsigned int timeParseWeekdays(const char* arg) {
    char day[4] = {0};
    char* err = NULL;
    unsigned int i = 0;
    unsigned int ret = 0;
    bool valid = false;

    while(myStrseg(day, sizeof(day), &arg, ',') != NULL) {
        i = strtoul(day, &err, 0);
        if(*err == '\0') {
            if((i == 0) || (i > 7)) {
                SAH_TRACEZ_ERROR(ME, "No, the week does NOT begin with Sunday.: %d", i);
            }
            ret |= 1 << i;
            continue;
        }

        valid = false;
        for(i = 1; i < ARRAY_SIZE(week_days); ++i) {
            if(strncmp(day, week_days[i], 2) == 0) {
                ret |= 1 << i;
                valid = true;
            }
        }

        if(!valid) {
            SAH_TRACEZ_ERROR(ME, "%s is not a valid day specifier", day);
        }
    }

    return ret;
}

static void iptfSetInterfaceMask(unsigned char* mask, const char* interface) {
    int len = strlen(interface);
    memset(mask, 0, IFNAMSIZ);
    memset(mask, 0xFF, (len + 1 > IFNAMSIZ) ? IFNAMSIZ : len + 1);
}

struct xtc_handle* get_iptc_handle(const char* table, bool is_ipv6) {
    struct xtc_handle** handle = NULL;
    bool prior_lock = LOCKED;

    when_str_empty_trace(table, exit, ERROR, "Missing table");

    if(strcmp(table, "filter") == 0) {
        handle = is_ipv6 ? &h6_filter : &h_filter;
    } else if(strcmp(table, "nat") == 0) {
        if(is_ipv6) {
#if !(LINUX_VERSION_CODE > KERNEL_VERSION(3, 9, 0))
            if(ipv6_nat_support) {
                SAH_TRACEZ_WARNING(ME, "ipv6 redirect not supported for this kernel version");
            }
            ipv6_nat_support = false;
#endif
            when_false(ipv6_nat_support, exit);
        }
        handle = is_ipv6 ? &h6_nat : &h_nat;
    } else if(strcmp(table, "raw") == 0) {
        handle = is_ipv6 ? &h6_raw : &h_raw;
    } else if(strcmp(table, "mangle") == 0) {
        handle = is_ipv6 ? &h6_mangle : &h_mangle;
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown table: '%s'", table);
        goto exit;
    }

    if((handle != NULL) && (*handle != NULL)) {
        if(!prior_lock) {
            SAH_TRACEZ_ERROR(ME, "Bug: batch ongoing without owning lock file");
            handle = NULL;
        }
        goto exit;
    }

    when_false(lock(), exit);

    if(is_ipv6) {
        *handle = ip6tc_init(table);
    } else {
        *handle = iptc_init(table);
    }

    if(*handle == NULL) {
        SAH_TRACEZ_ERROR(ME, "%s_init(%s) failed: '%s'", is_ipv6 ? "ip6tc" : "iptc", table, iptc_strerror(errno));
        if(!prior_lock) { // don't unlock if the lock was not aqcuired now
            unlock();
        }
        goto exit;
    }

exit:
    return handle == NULL ? NULL : *handle;
}

static bool add_nat_target(struct iptc_rule* r, const fw_rule_t* const rule) {
    fw_rule_target_t target = 0;
    bool ipv4 = false;
    struct in_addr addr = {0};
    bool dnat = false;
    const char* cidr = NULL;
    char* host = NULL;
    char* slash = NULL;
    uint32_t min_port = 0;
    uint32_t max_port = 0;
    bool rv = false;

    ipv4 = fw_rule_get_ipv4(rule);
    when_false(ipv4, exit); // legacy code uses struct in_addr (which is for ipv4)

    target = fw_rule_get_target(rule);
    if(target == FW_RULE_TARGET_DNAT) {
        when_failed_trace(fw_rule_get_target_dnat_options(rule, &cidr, &min_port, &max_port), exit, ERROR, "DNAT: Failed to get target options");
        dnat = true;
    } else if(target == FW_RULE_TARGET_SNAT) {
        when_failed_trace(fw_rule_get_target_snat_options(rule, &cidr, &min_port, &max_port), exit, ERROR, "SNAT: Failed to get target options");
        dnat = false;
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown NAT target: %d", target);
        goto exit;
    }

    when_null_trace(cidr, exit, ERROR, "%cNAT: No target ip", dnat ? 'D' : 'S'); // (for DNAT) ipaddress is optional
    host = strdup(cidr);
    when_null(host, exit);

    slash = strchr(host, '/');
    if(slash != NULL) {
        const int max_prefixlen_ipv4 = 32;
        when_false_trace((atoi(slash + 1) == max_prefixlen_ipv4), exit, ERROR, "%cNAT: Cannot handle block of addresses: %s", dnat ? 'D' : 'S', cidr);
        *slash = '\0'; // for inet_pton
    }

    when_false_trace(inet_pton(AF_INET, host, &addr) == 1, exit, ERROR, "%cNAT: Address conversion failed: %s", dnat ? 'D' : 'S', host);

    if(dnat) {
        iptc_rule_add_dnat_target(r, addr.s_addr, min_port, max_port);
    } else {
        iptc_rule_add_snat_target(r, addr.s_addr, min_port, max_port);
    }
    rv = true;

exit:
    free(host);
    return rv;
}

struct iptc_rule* iptc_create_rule(const fw_rule_t* const rule) {
    int retval = 0;
    const char* source = fw_rule_get_source(rule);
    const char* source_mask = fw_rule_get_source_mask(rule);
    const char* source_set = fw_rule_get_source_ipset(rule);
    const char* in = fw_rule_get_in_interface(rule);
    const char* dest = fw_rule_get_destination(rule);
    const char* dest_set = fw_rule_get_destination_ipset(rule);
    const char* dest_mask = fw_rule_get_destination_mask(rule);
    const char* source_mac = fw_rule_get_source_mac_address(rule);
    const char* out = fw_rule_get_out_interface(rule);
    const char* sstr = fw_rule_get_filter_string(rule);
    const char* timeStart = fw_rule_get_filter_start_time(rule);
    const char* timeStop = fw_rule_get_filter_end_time(rule);
    const char* weekDays = fw_rule_get_filter_weekdays(rule);
    uint32_t dest_port = fw_rule_get_destination_port(rule);
    uint32_t dest_port_max = fw_rule_get_destination_port_range_max(rule);
    uint32_t protocol = fw_rule_get_protocol(rule);
    uint32_t source_port = fw_rule_get_source_port(rule);
    uint32_t source_port_max = fw_rule_get_source_port_range_max(rule);
    bool exclude_in = fw_rule_get_in_interface_excluded(rule);
    bool exclude_source = fw_rule_get_source_excluded(rule);
    bool exclude_source_set = fw_rule_get_source_ipset_excluded(rule);
    bool exclude_source_port = fw_rule_get_source_port_excluded(rule);
    bool exclude_source_mac = fw_rule_get_source_mac_excluded(rule);
    bool exclude_out = fw_rule_get_out_interface_excluded(rule);
    bool exclude_dest = fw_rule_get_destination_excluded(rule);
    bool exclude_dest_set = fw_rule_get_destination_ipset_excluded(rule);
    bool exclude_dest_port = fw_rule_get_destination_port_excluded(rule);
    bool exclude_protocol = fw_rule_get_protocol_excluded(rule);
    fw_rule_target_t target = fw_rule_get_target(rule);
    fw_rule_policy_t policy = fw_rule_get_target_policy_option(rule);
    uint32_t target_dscp = fw_rule_get_target_dscp_option(rule);
    uint32_t target_pbit = fw_rule_get_target_pbit_option(rule);
    const char* target_chain = fw_rule_get_target_chain_option(rule);
    uint32_t ip_length_min = fw_rule_get_ip_min_length(rule);
    uint32_t ip_length_max = fw_rule_get_ip_max_length(rule);
    bool exclude_ip_length = false;
    uint32_t dscp = fw_rule_get_dscp(rule);
    bool exclude_dscp = fw_rule_get_dscp_excluded(rule);
    bool ipv4 = fw_rule_get_ipv4(rule);
    const char* ctstate = fw_rule_get_conntrack_state(rule);
    uint64_t connbytes_min = fw_rule_get_connbytes_min(rule);
    uint64_t connbytes_max = fw_rule_get_connbytes_max(rule);
    const char* connbytes_param = fw_rule_get_connbytes_param(rule);
    const char* connbytes_direction = fw_rule_get_connbytes_direction(rule);
    uint32_t skb_mark = fw_rule_get_skb_mark(rule);
    uint32_t skb_mark_mask = fw_rule_get_skb_mark_mask(rule);
    uint32_t conn_mark = fw_rule_get_conn_mark(rule);
    uint32_t conn_mark_mask = fw_rule_get_conn_mark_mask(rule);
    bool exclude_skb_mark = fw_rule_get_skb_mark_excluded(rule);
    bool exclude_conn_mark = fw_rule_get_conn_mark_excluded(rule);
    uint32_t target_mark = 0;
    uint32_t target_mask = 0;
    uint32_t target_class = 0;
    struct iptc_rule* r = NULL;

    r = iptc_rule_new(ipv4);
    when_null(r, exit);

    if(ctstate != NULL) {
        iptc_rule_add_state_match(r, ctstate);
    }

    if((connbytes_param != NULL) && (connbytes_direction != NULL)) {
        iptc_rule_add_connbytes_match(r, connbytes_min, connbytes_max, connbytes_param, connbytes_direction);
    }

    if(skb_mark_mask != 0) {
        iptc_rule_add_skbmark_match(r, skb_mark, skb_mark_mask, (exclude_skb_mark) ? 1 : 0);
    }

    if(conn_mark_mask != 0) {
        iptc_rule_add_connmark_match(r, conn_mark, conn_mark_mask, (exclude_conn_mark) ? 1 : 0);
    }

    if(!STRING_EMPTY(source_set)) {
        iptc_rule_add_set_match(r, source_set, true, exclude_source_set);
    }

    if(!STRING_EMPTY(dest_set)) {
        iptc_rule_add_set_match(r, dest_set, false, exclude_dest_set);
    }

    if(ipv4) {
        if(!STRING_EMPTY(in)) {
            strncpy(r->e.entry->ip.iniface, in, IFNAMSIZ - 1);
            r->e.entry->ip.iniface[IFNAMSIZ - 1] = 0;
            iptfSetInterfaceMask(r->e.entry->ip.iniface_mask, r->e.entry->ip.iniface);
            r->e.entry->ip.invflags |= (exclude_in) ? IPT_INV_VIA_IN : 0;
        }
        if(!STRING_EMPTY(out)) {
            strncpy(r->e.entry->ip.outiface, out, IFNAMSIZ - 1);
            r->e.entry->ip.outiface[IFNAMSIZ - 1] = 0;
            iptfSetInterfaceMask(r->e.entry->ip.outiface_mask, r->e.entry->ip.outiface);
            r->e.entry->ip.invflags |= (exclude_out) ? IPT_INV_VIA_OUT : 0;
        }
        if(STRING_EMPTY(dest_set) && !STRING_EMPTY(dest)) {
            retval = parsePrefix(dest, dest_mask, &(r->e.entry->ip.dst.s_addr), &(r->e.entry->ip.dmsk.s_addr));
            when_true(!retval, error);
            r->e.entry->ip.invflags |= (exclude_dest) ? IPT_INV_DSTIP : 0;
        }
        if(STRING_EMPTY(source_set) && !STRING_EMPTY(source)) {
            retval = parsePrefix(source, source_mask, &(r->e.entry->ip.src.s_addr), &(r->e.entry->ip.smsk.s_addr));
            when_true(!retval, error);
            r->e.entry->ip.invflags |= (exclude_source) ? IPT_INV_SRCIP : 0;
        }
        if(protocol != 0) {
            int32_t icmp_type = fw_rule_get_icmp_type(rule);
            r->e.entry->ip.proto = protocol;
            r->e.entry->ip.invflags |= (exclude_protocol) ? IPT_INV_PROTO : 0;
            r->e.entry->ip.flags = 0; /*see flag parameters in ip_tables.h*/

            if((IPPROTO_ICMP == protocol) && (ICMP_ECHOREPLY < icmp_type)) {
                int32_t icmp_code = fw_rule_get_icmp_code(rule);
                iptc_rule_add_icmp_match(r, (unsigned char) icmp_type, (unsigned char) icmp_code, 0);
            }
        }
    } else { //IPv6
        if(!STRING_EMPTY(in)) {
            strncpy(r->e.entry6->ipv6.iniface, in, IFNAMSIZ - 1);
            r->e.entry6->ipv6.iniface[IFNAMSIZ - 1] = 0;
            iptfSetInterfaceMask(r->e.entry6->ipv6.iniface_mask, r->e.entry6->ipv6.iniface);
            r->e.entry6->ipv6.invflags |= (exclude_in) ? IP6T_INV_VIA_IN : 0;
        }
        if(!STRING_EMPTY(out)) {
            strncpy(r->e.entry6->ipv6.outiface, out, IFNAMSIZ - 1);
            r->e.entry6->ipv6.outiface[IFNAMSIZ - 1] = 0;
            iptfSetInterfaceMask(r->e.entry6->ipv6.outiface_mask, r->e.entry6->ipv6.outiface);
            r->e.entry6->ipv6.invflags |= (exclude_out) ? IP6T_INV_VIA_OUT : 0;
        }
        if(STRING_EMPTY(dest_set) && !STRING_EMPTY(dest)) {
            retval = parsePrefix6(dest, dest_mask, &(r->e.entry6->ipv6.dst), &(r->e.entry6->ipv6.dmsk));
            when_true(!retval, error);
            r->e.entry6->ipv6.invflags |= (exclude_dest) ? IP6T_INV_DSTIP : 0;
        }
        if(STRING_EMPTY(source_set) && !STRING_EMPTY(source)) {
            retval = parsePrefix6(source, source_mask, &(r->e.entry6->ipv6.src), &(r->e.entry6->ipv6.smsk));
            when_true(!retval, error);
            r->e.entry6->ipv6.invflags |= (exclude_source) ? IP6T_INV_SRCIP : 0;
        }
        if(protocol != 0) {
            int32_t icmpv6_type = fw_rule_get_icmpv6_type(rule);
            r->e.entry6->ipv6.proto = protocol;
            r->e.entry6->ipv6.invflags |= (exclude_protocol) ? IP6T_INV_PROTO : 0;
            /*see flag parameters in ip6_tables.h*/
            r->e.entry6->ipv6.flags |= 0x01; /* Set if rule cares about upper protocols */

            if((IPPROTO_ICMPV6 == protocol) && (ICMP_ECHOREPLY < icmpv6_type)) {
                int32_t icmpv6_code = fw_rule_get_icmpv6_code(rule);
                iptc_rule_add_icmp6_match(r, (unsigned char) icmpv6_type, (unsigned char) icmpv6_code, 0);
            }
        }
    }

    iptc_rule_add_dscp_match(r, dscp, (exclude_dscp) ? 1 : 0);

    if(IPPROTO_TCP == protocol) {
        iptc_rule_add_tcp_match(r, dest_port,
                                dest_port_max,
                                (exclude_dest_port) ? XT_TCP_INV_DSTPT : 0,
                                source_port,
                                source_port_max,
                                (exclude_source_port) ? XT_TCP_INV_SRCPT : 0);

    } else if(IPPROTO_UDP == protocol) {
        iptc_rule_add_udp_match(r, dest_port,
                                dest_port_max,
                                (exclude_dest_port) ? XT_TCP_INV_DSTPT : 0,
                                source_port,
                                source_port_max,
                                (exclude_source_port) ? XT_TCP_INV_SRCPT : 0);
    }

    iptc_rule_add_length_match(r, ip_length_min, ip_length_max, exclude_ip_length);

    if(!STRING_EMPTY(source_mac)) {
        uint8_t smac[6];
        if(sscanf(source_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
                  &smac[0], &smac[1], &smac[2], &smac[3], &smac[4], &smac[5]) == 6) {
            iptc_rule_add_mac_match(r, smac, exclude_source_mac);
        }
    }

    if(!STRING_EMPTY(sstr)) {
        iptc_rule_add_string_match(r, sstr);
    }

    if(!STRING_EMPTY(timeStart) && !STRING_EMPTY(timeStop) && !STRING_EMPTY(weekDays)) {
        iptc_rule_add_time_match(r, timeParseMinutes(timeStart), timeParseMinutes(timeStop), timeParseWeekdays(weekDays));
    }

#ifdef SUPPORTS_PHYSDEV
    {
        const char* physdev_out = fw_rule_get_physdev_out(rule);
        const char* physdev_in = fw_rule_get_physdev_in(rule);
        bool exclude_physdev_out = fw_rule_get_physdev_out_excluded(rule);
        bool exclude_physdev_in = fw_rule_get_physdev_in_excluded(rule);
        if((physdev_out != NULL) && (*physdev_out != 0)) {
            iptc_rule_add_physdev_out_match(r, physdev_out, exclude_physdev_out);
        }
        if((physdev_in != NULL) && (*physdev_in != 0)) {
            iptc_rule_add_physdev_in_match(r, physdev_in, exclude_physdev_in);
        }
    }
#endif

    switch(target) {
    case FW_RULE_TARGET_RETURN:
        iptc_rule_add_verdict_target(r, -NF_REPEAT - 1);
        break;
    case FW_RULE_TARGET_MARK:
    {
        if(fw_rule_get_target_mark_options(rule, &target_mark, &target_mask) != 0) {
            goto error;
        } else {
            iptc_rule_add_mark_target(r, target_mark, target_mask);
        }
    }
    break;
    case FW_RULE_TARGET_CLASSIFY:
    {
        if(fw_rule_get_target_classify_option(rule, &target_class) != 0) {
            goto error;
        } else {
            iptc_rule_add_classify_target(r, target_class);
        }
    }
    break;
    case FW_RULE_TARGET_DSCP:
        iptc_rule_add_dscp_target(r, target_dscp);
        break;
    case FW_RULE_TARGET_QUEUE:
        SAH_TRACEZ_WARNING(ME, "Queue target not supported!");
        break;
    case FW_RULE_TARGET_PBIT:
        iptc_rule_add_pbit_target(r, target_pbit);
        break;
    case FW_RULE_TARGET_POLICY:
        switch(policy) {
        case FW_RULE_POLICY_DROP:
            iptc_rule_add_verdict_target(r, -NF_DROP - 1);
            break;
        case FW_RULE_POLICY_ACCEPT:
            iptc_rule_add_verdict_target(r, -NF_ACCEPT - 1);
            break;
        case FW_RULE_POLICY_MASQUERADE:
        {
            uint32_t min_port = 1024;
            uint32_t max_port = 65535;
            iptc_rule_add_masquerade_target(r, min_port, max_port);
        }
        break;
        case FW_RULE_POLICY_REJECT:
            iptc_rule_add_reject_target(r, (protocol == 6) ? IPT_TCP_RESET : IPT_ICMP_PORT_UNREACHABLE);
            break;
        case FW_RULE_POLICY_LAST: //should never get here.
            SAH_TRACEZ_ERROR(ME, "Unhandled policy %d", target);
            goto error;
        }
        break;
    case FW_RULE_TARGET_CHAIN:
        iptc_rule_add_jump_target(r, target_chain);
        break;
    case FW_RULE_TARGET_DNAT:     // fallthrough to SNAT
    case FW_RULE_TARGET_SNAT:
        when_false(add_nat_target(r, rule), error);
        break;
    case FW_RULE_TARGET_REDIRECT:
    {
        if(ipv4) {
            iptc_rule_add_redirect_target(r);
        } else {
            iptc_rule_add_redirect6_target(r);
        }
    }
    break;
    case FW_RULE_TARGET_NFQUEUE:
    {
        unsigned int qnum = 0;
        unsigned int qtotal = 0;
        unsigned int flags = 0;
        fw_rule_get_target_nfqueue_options(rule, &qnum, &qtotal, &flags);
        iptc_rule_add_nfqueue_target(r, qnum, qtotal, flags);
    }
    break;
    case FW_RULE_TARGET_NFLOG:
    {
        unsigned int gnum = 0;
        unsigned int len = 0;
        unsigned int threshold = 0;
        unsigned int flags = 0;
        const char* prefix = "";
        fw_rule_get_target_nflog_options(rule, &gnum, &len, &threshold, &flags, &prefix);
        iptc_rule_add_nflog_target(r, gnum, len, threshold, flags, prefix);
    }
    break;
    case FW_RULE_TARGET_LOG:
    {
        unsigned char level = LOG_ERR;
        unsigned char logflags = 0;
        const char* prefix = "";
        fw_rule_get_target_log_options(rule, &level, &logflags, &prefix);
        iptc_rule_add_log_target(r, level, logflags, prefix);
    }
    break;
    default:
        SAH_TRACEZ_ERROR(ME, "Unhandled target %d", target);
        goto error;
    }

exit:
    return r;
error:
    iptc_rule_delete(r, fw_rule_get_ipv4(rule));
    return NULL;
}

int iptc_apply(void) {
    int retval = 1;

    if(h_filter != NULL) {
        if(iptc_commit(h_filter) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit filter Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit filter Succeeded");
        }
        iptc_free(h_filter);
        h_filter = NULL;
    }
    if(h_nat != NULL) {
        if(iptc_commit(h_nat) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit nat Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit nat Succeeded");
        }
        iptc_free(h_nat);
        h_nat = NULL;
    }
    if(h_raw != NULL) {
        if(iptc_commit(h_raw) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit raw Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit raw Succeeded");
        }
        iptc_free(h_raw);
        h_raw = NULL;
    }
    if(h_mangle != NULL) {
        if(iptc_commit(h_mangle) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit mangle Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit mangle Succeeded");
        }
        iptc_free(h_mangle);
        h_mangle = NULL;
    }

    if(h6_filter != NULL) {
        if(ip6tc_commit(h6_filter) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit IPv6 filter Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit IPv6 filter Succeeded");
        }
        ip6tc_free(h6_filter);
        h6_filter = NULL;
    }
    if(h6_raw != NULL) {
        if(ip6tc_commit(h6_raw) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit IPv6 raw Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit IPv6 raw Succeeded");
        }
        ip6tc_free(h6_raw);
        h6_raw = NULL;
    }
    if(h6_nat != NULL) {
        if(ip6tc_commit(h6_nat) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit IPv6 nat Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit IPv6 nat Succeeded");
        }
        ip6tc_free(h6_nat);
        h6_nat = NULL;
    }
    if(h6_mangle != NULL) {
        if(ip6tc_commit(h6_mangle) == 0) {
            retval = 0;
            SAH_TRACEZ_WARNING(ME, "Commit IPv6 nat Failed[%s]", iptc_strerror(errno));
        } else {
            SAH_TRACEZ_INFO(ME, "Commit IPv6 nat Succeeded");
        }
        ip6tc_free(h6_mangle);
        h6_mangle = NULL;
    }

    unlock();

    return retval;
}
