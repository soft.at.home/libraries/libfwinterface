include ../makefile.inc

# Selected interface
ifeq ($(CONFIG_SAH_LIB_FWINTERFACE_INTERFACE_DUMMY),y)
INTERFACE = interface_dummy
else ifeq ($(CONFIG_SAH_LIB_FWINTERFACE_INTERFACE_IPTC),y)
INTERFACE = interface_iptc
LDFLAGS += $(shell pkg-config --libs libiptc)
else
$(warning "No interface selected in the configuration. Default to IPTC")
INTERFACE = interface_iptc
LDFLAGS += $(shell pkg-config --libs libiptc)
endif

# build destination directories
OBJDIR = ../output/$(MACHINE)

# TARGETS
TARGET_SO = $(OBJDIR)/$(COMPONENT).so.$(VERSION)
TARGET_A = $(OBJDIR)/$(COMPONENT).a

# directories
# source directories
SRCDIR = .
INCDIR_PUB = ../include
INCDIR_PRIV = ../include_priv
INCDIRS = $(INCDIR_PUB) $(INCDIR_PRIV) $(if $(STAGINGDIR), $(STAGINGDIR)/include $(STAGINGDIR)/usr/include)
STAGING_LIBDIR = $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib -L$(STAGINGDIR)/usr/lib)

# files
HEADERS = $(wildcard $(INCDIR_PUB)/fwinterface/*.h)
SOURCES = $(wildcard $(SRCDIR)/$(INTERFACE)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

# compilation and linking flags
CFLAGS += -Werror -Wall -Wextra \
          -Wformat=2 -Wshadow \
          -Wwrite-strings -Wredundant-decls \
          -Wmissing-declarations \
          -Wno-format-nonliteral \
          -fPIC -g3 $(addprefix -I ,$(INCDIRS)) \
          -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

ifeq ($(CC_NAME),g++)
	CFLAGS += -std=c++2a -fpermissive
else
	CFLAGS += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11 -D_GNU_SOURCE
endif
ifeq ($(CONFIG_SAH_LIB_FWINTERFACE_SUPPORTS_PHYSDEV),y)
    CFLAGS += -DSUPPORTS_PHYSDEV
else
    SOURCES := $(filter-out $(SRCDIR)/$(INTERFACE)/iptc_matches_physdev.c, $(SOURCES))
endif

LDFLAGS += $(STAGING_LIBDIR) -shared -fPIC -lamxc -lsahtrace -lfwrules

# targets
all: $(TARGET_SO) $(TARGET_A)

$(TARGET_SO): $(OBJECTS)
	$(CC) -Wl,-soname,$(COMPONENT).so.$(VMAJOR) -o $(@) $(OBJECTS) $(LDFLAGS)

$(TARGET_A): $(OBJECTS)
	$(AR) rcs $(@) $^

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/$(INTERFACE)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/ ../$(COMPONENT)-*.* ../$(COMPONENT)_*.*

.PHONY: all clean
