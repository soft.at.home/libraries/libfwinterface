# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.6.8 - 2024-07-09(09:17:22 +0000)

### Other

- PRPL/COVERITY: libfwinterface issue

## Release v1.6.7 - 2024-06-17(15:29:56 +0000)

### Other

- - [PRPL][4.0.12.12][SIP ALG]SIP ALG is on even when switch shows disabled

## Release v1.6.6 - 2024-06-13(12:44:52 +0000)

### Other

- match every day and have no date-start or date-end boundary

## Release v1.6.5 - 2024-06-11(12:54:49 +0000)

### Other

- DMZ rule not created

## Release v1.6.4 - 2024-05-28(08:02:58 +0000)

### Fixes

- add support for iptc extension string and time

## Release v1.6.3 - 2024-05-24(12:01:45 +0000)

### Fixes

- Add support for ICMP code when creating iptc rule

## Release v1.6.2 - 2024-04-25(11:15:28 +0000)

### Fixes

- remove unused malloc

## Release v1.6.1 - 2024-04-25(10:35:34 +0000)

### Fixes

- support [SD]NAT target option ip cidr format

## Release v1.6.0 - 2024-03-20(17:15:05 +0000)

### New

- add support for iptc extension string and time

## Release v1.5.7 - 2024-03-07(14:21:45 +0000)

### Changes

- [PacketInterception] Add Flowmonitor support

## Release v1.5.6 - 2024-03-05(14:53:36 +0000)

### Changes

- [PacketInterception] Add Flowmonitor support

## Release v1.5.5 - 2024-02-20(08:16:18 +0000)

### Other

- - [Firewall][libfwrules] - Second optimization of fw_commit() function

## Release v1.5.4 - 2024-02-19(09:13:01 +0000)

### Changes

- [PacketInterception] Add NFLOG support

## Release v1.5.3 - 2024-02-09(08:02:03 +0000)

### Other

- [QOS]Support hardware based QoS

## Release v1.5.2 - 2023-12-21(07:45:40 +0000)

### Other

- [QOS]Support hardware based QoS

## Release v1.5.1 - 2023-12-20(15:24:43 +0000)

### Other

- [QOS]Support hardware based QoS

## Release v1.5.0 - 2023-11-23(12:29:43 +0000)

### New

- [FIrewall][SIP ALG][DM] It must be possible to have a enable/disable SIP ALG using the HL API

## Release v1.4.0 - 2023-11-19(08:32:54 +0000)

### New

-  [TR-181-Firewall] Add the possibility to LOG functionality in the firewall

## Release v1.3.14 - 2023-05-10(07:57:34 +0000)

### Fixes

- Firewall.Chain.i.Rule.i.SourceMAC doesn't match MAC address when Protocol!=-1

## Release v1.3.13 - 2023-02-24(15:09:33 +0000)

### Other

- Dependency on libiptc is not valid for openwrt22

## Release v1.3.12 - 2023-02-01(09:43:25 +0000)

### Other

- Add missing iptables conmark extension dependency

## Release v1.3.11 - 2022-11-09(08:53:43 +0000)

### Other

- [Packet Interception] Create the new Packet Interception component

## Release v1.3.10 - 2022-10-14(08:49:21 +0000)

### Fixes

- - [libfwinterface] Fix unused variable

## Release v1.3.9 - 2022-09-15(10:17:05 +0000)

### Fixes

- - [ibsen][mtk] Mediatek config for QoS

## Release v1.3.8 - 2022-09-09(12:09:35 +0000)

### Fixes

- Add missing dependencies

## Release v1.3.7 - 2022-08-25(08:26:07 +0000)

### Changes

- - Index of insertion too big

## Release v1.3.6 - 2022-05-18(10:36:02 +0000)

### Other

- Correct dependencies

## Release v1.3.5 - 2022-03-29(10:35:53 +0000)

### Fixes

- RAPlugin must configure the Firewall

## Release v1.3.4 - 2022-02-02(10:52:59 +0000)

### Other

- [CI] Fix BUILD_DEPS for debian bullseye

## Release v1.3.3 - 2022-01-26(12:45:41 +0000)

### Other

- move ClassificationKey to queues and add interface parameter to scheduler

## Release v1.3.2 - 2022-01-25(11:00:57 +0000)

### Other

- - [libfwinterface] Document code
- - [libfwinterface] Document code

## Release v1.3.1 - 2022-01-03(14:46:27 +0000)

### Changes

- Use BSD-2-Clause-Patent license

## Release v1.3.0 - 2021-10-25(11:02:52 +0000)

### New

- support target NFQUEUE

## Release v1.2.1 - 2021-09-30(09:02:45 +0000)

### Fixes

- undeclared symbol IP_CT_NEW

## Release v1.2.0 - 2021-09-24(08:38:30 +0000)

### New

- match rules by connection state

## Release v1.1.9 - 2021-09-07(12:28:44 +0000)

### Other

- implement policy masquerade

## Release v1.1.8 - 2021-08-24(13:44:01 +0000)

### Fixes

- libfwinterface misidentifies rules as duplicates

## Release v1.1.7 - 2021-08-04(10:40:17 +0000)

### Fixes

- [libfwinterface] Check rule before adding it to the firewall

### Other

- - [libfwinterface][iptc] Check if a firewall rule exists before an insert or append

## Release v1.1.6 - 2021-08-04(10:17:26 +0000)

### Fixes

- [libfwinterface] Pipeline doesn't stop when memory leaks are detected

### Other

- Issue: soft.at.home/libraries/libfwinterface#12 baf: add libiptc as a compile and runtime dependency
- - [Gitlab CI][Unit tests][valgrind] Pipelines don't stop when memory leaks are detected

## Release v1.1.5 - 2021-07-27(06:47:57 +0000)

- Issue: soft.at.home/libraries/libfwinterface#11 Component.dep: add libfwrules

## Release v1.1.4 - 2021-07-26(14:13:44 +0000)

- Issue: soft.at.home/libraries/libfwinterface#10 baf: fix libfwrules

## Release v1.1.3 - 2021-07-26(12:48:13 +0000)

- Issue: soft.at.home/libraries/libfwinterface#9 toolchain: _GNU_SOURCE must be defined

## Release v1.1.2 - 2021-07-20(11:21:18 +0000)

- Issue: soft.at.home/libraries/libfwinterface#6 fw_apply: a return value must be added
- Issue: soft.at.home/libraries/libfwinterface#7 icmp: fix match targets in iptc_create_rule
- Issue: soft.at.home/libraries/libfwinterface#8 iptc: add revision 2 to target mark

## Release v1.1.1 - 2021-07-19(13:40:40 +0000)

- Issue: soft.at.home/libraries/libfwinterface#5 fw_add_chain: do not fail when a chain exists

## Release v1.1.0 - 2021-07-19(12:21:44 +0000)

- Issue: soft.at.home/libraries/libfwinterface#4 interface: add function to append a rule to the firewall

## Release v1.0.3 - 2021-07-15(07:10:57 +0000)

- Issue: soft.at.home/libraries/libfwinterface#3 baf: missing prorietary parameter

## Release v1.0.2 - 2021-07-14(18:11:23 +0000)

- Issue: soft.at.home/libraries/libfwinterface#2 Fix changelog

## Release v1.0.1 - 2021-07-14(13:18:53 +0000)

- Issue: soft.at.home/libraries/libfwinterface#1 Fix baf.yml

## Release v1.0.0 - 2021-07-08(11:36:44 +0000)

- Merge 'dev_init' into 'main'

